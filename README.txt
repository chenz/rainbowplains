RAINBOW PLAINS
==============
  Someone hid a bunch of mines in the rainbow plains.
  Can you find them all?


Overview:
=========
  Rainbow Plains is a mine sweeping game developed 
  primarily for the GP2X Wiz.


Controls (Wiz):
===============
  You can use either touchscreen or button contols.

  Touchscreen:

  CLICK .............. Action
  CLICK + SHOULDER ... Flag

  Buttons:

  A, B ............... Action
  X, Y ............... Back, Flag
  SHOULDER ........... Flag
  MENU ............... In game menu
  MENU + SELECT ...... Quit game


Controls (PC):
==============
  You can use either mouse or keyboard contols.

  Mouse:
  
  LEFT CLICK ......... Action
  LEFT CLICK + D ..... Flag

  Keys:

  A .................. Action
  S .................. Back, Flag
  D .................. Flag
  ESC ................ In game menu
  ALT + F4 ........... Quit game


BGM:
====
  Currently, no background music is included in the Rainbow Plains
  package. 

  You can however put your own music files in the "bgm" folder.

  Another option is to modify the search path in the save file
  "data/properties.dat" (this file is not part of the package, 
  it is created when Rainbow Plains first runs), for example:

    bgm.path.entry1=../bgm
    bgm.path.entry2=/mnt/sd/my-music-folder

  Path entries can be either absolute, or relative to the binary.
  All path entries will be scanned recursively for any files 
  with supported formats:

  * Ogg-Vorbis (.ogg)
  * Soundtracker (.mod)
  * Fastracker (.xm)
  * Screamtracker (.it)


Building (Wiz):
===============
  Example environment script for official GPH SDK:

--8<-- env.sh --------------------------------------------------
export SDK_HOME=/path/to/GPH_SDK
export SDK_INCLUDE="-I${SDK_HOME}/include -I${SDK_HOME}/DGE/include -I${SDK_HOME}/DGE/include/SDL"
export SDK_LIB="-L${SDK_HOME}/lib/target -L${SDK_HOME}/DGE/lib/target"
export PATH=$PATH:${SDK_HOME}/tools/gcc-4.0.2-glibc-2.3.6/arm-linux/bin
export CROSS_COMPILE=arm-linux-	
--8<------------------------------------------------------------

  Example Makefile.cfg for libstm:

--8<-- lib/libtmepp/src/Makefile.cfg ---------------------------
CXX=$(CROSS_COMPILE)g++
AR=$(CROSS_COMPILE)ar
LD=$(CROSS_COMPILE)ld

CXXFLAGS = \
	-Wall -O2 -g -fno-exceptions -mcpu=arm926ej-s -ffast-math -fomit-frame-pointer \
	$(SDK_INCLUDE) \
--8<-- lib/libtmepp/src/Makefile.cfg ---------------------------

  Example Makefile.cfg for libtmepp:

--8<-- lib/libtmepp/src/Makefile.cfg ---------------------------
USE_RENDERER_SDL = 0
USE_RENDERER_GL = 0
USE_RENDERER_WIZ = 1

CXX=$(CROSS_COMPILE)g++
AR=$(CROSS_COMPILE)ar
LD=$(CROSS_COMPILE)ld

CXXFLAGS = \
	-Wall -O2 -g -fno-exceptions -mcpu=arm926ej-s -ffast-math -fomit-frame-pointer \
	-I../../tinyxml \
	-DTIXML_USE_STL=1 \
	$(SDK_INCLUDE) \

LDFLAGS = \
	-L../../tinyxml -ltinyxml \
	$(SDK_LIB) \
	-lSDL \
	-lts \
	-lopengles_lite -lglport \
	-lpng -lz \
	-lm -lrt
--8<------------------------------------------------------------

  Example Makefile.cfg for Rainbow Plains:

--8<-- src/Makefile.cfg ----------------------------------------
DEBUG = 1

CXXFLAGS += \
	-mcpu=arm926ej-s -ffast-math -fomit-frame-pointer \
	-Wabi -fno-exceptions \
	$(SDK_INCLUDE) \
	-DSYSTEM_WIZ \

LDFLAGS += $(SDK_LIB) \
	-lSDL -lSDL_mixer \
	-lsmpeg -lts \
	-lpng -lz \
	-lopengles_lite -lglport \
	-lm -lrt \
	-lpcrecpp -lpcre 
--8<------------------------------------------------------------

  After the "Makefile.cfg"s have been created, run "make" in the lib and src folders.


Credits:
========
  Copyright (c) 2010-2011 Christian Henz <chrhenz at gmx dot de>
  Licensed under the GNU General Public License V2 (see GPL-2.txt for details)
  https://code.google.com/p/rainbowplains/

  pollux_set
  Copyright (c) Gra�vydas "notaz" Ignotas, 2009
  http://www.gp32x.com/board/index.php?/topic/48575-wiz-ram-timings-test/


Thanks:
=======
  Thanks go out to all "beta testers" on the gp32x.com forums! 
  (http://www.gp32x.com/board/index.php?/topic/54881-rainbow-plains-beta)  
  
  * bowz
  * Dj Co2
  * Jan-Nik
  * Mr.TwisT
  * Neil L
  * u9i
  
  Thanks for your comments and suggestions!
