#!/bin/bash

set -e

VERSION=$(make -s -C src version)

TMPDIR=$(mktemp -d)
OLDWD=$(pwd)

svn export $(svnpath) ${TMPDIR}/rainbowplains-src-${VERSION}
cd $TMPDIR
tar zcf rainbowplains-src-${VERSION}.tar.gz rainbowplains-src-${VERSION}
mv rainbowplains-src-${VERSION}.tar.gz ${OLDWD}/
cd $OLDWD
