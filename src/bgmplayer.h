#ifndef __BGMPLAYER_H__
#define __BGMPLAYER_H__

#include <string>
#include <vector>

#include <SDL_mixer.h>

class BGMPlayer
{
 public:

  enum State {

    Off, 
    On,
  };

  BGMPlayer();
  ~BGMPlayer();

  bool init();

  void play();
  void stop();
  void next();

  void update();

  std::string getCurrentFilename() const;

 private:
  void scanDir( const char* dirName );
  void load();
  void unload();

  State _state;
  int _currentFile;
  Mix_Music* _currentMusic;
  std::vector< std::string > _files;
};

#endif // __BGMPLAYER_H__
