#include "hue.h"

Hue::Hue( unsigned degree )
{
  setHue( degree );
}

void Hue::setHue( unsigned d )
{
  const uint8_t threshold = 255 - 59 * 4;

  d %= 360;
  this->degree = d;

  unsigned phase = d / 60;
  uint8_t index = d % 60;

  switch( phase ) {

  case 0: // Green up
    red = 255;
    green = threshold + index * 4;
    blue = threshold;
    break;

  case 1: // Red down
    red = 255 - index * 4;
    green = 255;
    blue = threshold;
    break;

  case 2: // Blue up
    red = threshold;
    green = 255;
    blue = threshold + index * 4;
    break;

  case 3: // Green down
    red = threshold;
    green = 255 - index * 4;
    blue = 255;
    break;

  case 4: // Red up
    red = threshold + index * 4;
    green = threshold;
    blue = 255;
    break;

  case 5: // Blue down
    red = 255;
    green = threshold;
    blue = 255 - index * 4;
    break;
  }
}

void Hue::shiftHue( unsigned degree )
{
  setHue( this->degree + degree );
}
