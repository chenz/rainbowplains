#ifndef __HIGHSCORESCREEN_H__
#define __HIGHSCORESCREEN_H__

#include "button.h"
#include "gamekey.h"
#include "highscores.h"
#include "menu.h"
#include "state.h"

class HighscoreScreen : public State
{
 public:
  HighscoreScreen( STM::StateManager* manager );

  void setGameKey( const GameKey& key );

  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  //void enter();
  
  void onTransition( STM::State* prevState );

 private:
  GameKey _gameKey;

  int _ticks;

  enum State {

    Delay,
    Display,
    FadeOut,
  };

  void setState( State state);

  State _state;

  enum {

    ButtonPrev,
    ButtonClose,
    ButtonNext,
  };

  Button _buttons[ 3 ];
  int _currentButton;
};

#endif // __HIGHSCORESCREEN_H__
