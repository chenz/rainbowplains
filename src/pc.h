#ifndef _PC_H_
#define _PC_H_

#if defined(SYSTEM_PC)

#include "system.h"

class SystemPC : public AbstractSystem
{
public:  
  int mapJoyButton( unsigned button ) const;
  int mapKey( unsigned key ) const;
  void enableScreensaver( bool on );
  void dbgShowStats();
};

#endif // defined(SYSTEM_PC)

#endif /* _PC_H_ */
