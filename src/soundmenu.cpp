#include "bgmplayer.h"
#include "game.h"
#include "globals.h"
#include "menu.h"
#include "options.h"
#include "propertiesfile.h"
#include "soundmenu.h"

#include <algorithm>
#include <cctype>

//1           SOUND SETTINGS
//2 [<<] SFX:        ON/OFF         [>>]
//3 [<<] SFX VOLUME: 10             [>>]
//4 [<<] BGM:        ON/OFF         [>>]
//5 [<<] BGM VOLUME: 10             [>>]
//6 [<<] BGM MODE:   SHUFFLE/NORMAL [>>]
//7      BGM: FOOBAR - ASDQWERTZ.OGG
//8         [SKIP]     [BACK]

const int FontW = 10;
const int FontH = 10;
const int Padding = 8;
const int Spacing = 4;

const int ButtonH = 24;
const int RowH = ButtonH + Spacing;

const int UpperButtonW = Menu::roundSize(2 * Padding + 2 * FontW);

const int NumRows = 8;

// "BGM MODE:   SHUFFLE": 19 chars
const int UpperMaxChars = 19;

const int UpperRowW = UpperMaxChars * FontW + 2 * Spacing + 2 * UpperButtonW;

const int UpperX0 = (ScreenW - UpperRowW) / 2;
const int UpperX1 = UpperX0 + UpperButtonW + Spacing;
const int UpperX2 = UpperX0 + UpperButtonW + Spacing + UpperMaxChars * FontW + Spacing;

const int BoxW = Menu::roundSize(std::max( UpperRowW + 4 * Spacing, 25 * FontW + 4 * Spacing ));
const int BoxH = Menu::roundSize(NumRows * RowH + Padding);
const int BoxX = (ScreenW - BoxW) / 2;
const int BoxY = (ScreenH - BoxH) / 2;

SoundMenu::SoundMenu( STM::StateManager* manager )
  : ::State(manager)
  , listener(0)
  , _currentButton(B_BGM_SKIP)
{

  int y = BoxY + RowH;

  _buttons[ B_SFX_TOGGLE_PREV ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
  _buttons[ B_SFX_TOGGLE_NEXT ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
  y += RowH;

  _buttons[ B_SFX_VOLUME_PREV ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
  _buttons[ B_SFX_VOLUME_NEXT ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
  y += RowH;

  _buttons[ B_BGM_TOGGLE_PREV ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
  _buttons[ B_BGM_TOGGLE_NEXT ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
  y += RowH;

  _buttons[ B_BGM_VOLUME_PREV ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
  _buttons[ B_BGM_VOLUME_NEXT ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
  y += RowH;

  _buttons[ B_BGM_MODE_PREV ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
  _buttons[ B_BGM_MODE_NEXT ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
  y += RowH;
  y += RowH;

  const int LowerButtonW = Menu::roundSize(2 * Padding + 8 * FontW);
  const int LowerRowW = 2 * LowerButtonW + Spacing;
  const int LowerButtonX0 = (ScreenW - LowerRowW) / 2;
  const int LowerButtonX1 = LowerButtonX0 + LowerButtonW + Spacing;

  _buttons[ B_BGM_SKIP ] = Button( Tme::FloatRect( LowerButtonX0, y, LowerButtonW, ButtonH ), "SKIP" );
  _buttons[ B_BACK ] = Button( Tme::FloatRect( LowerButtonX1, y, LowerButtonW, ButtonH ), "BACK" );
  y += RowH;
}

const int FadeTicks = 500 / Period;

void SoundMenu::update()
{
  for( int i = 0; i < NUM_BUTTONS; ++i )
    _buttons[ i ].update();

  _ticks++;

  switch( _state ) {

  case FadeIn:
    if( _ticks >= FadeTicks ) {
 
      setTransitionState(TransitionInDone);
      setState(Active);
    }
    break;

  case Active:
    break;

  case FadeOut:
    if( _ticks >= FadeTicks ) {
      
      setTransitionState(TransitionOutDone);
    }
    break;
  }
}

std::string strToUpper( const std::string& sIn )
{
  std::string sOut(sIn);
  std::transform( sOut.begin(), sOut.end(), sOut.begin(), toupper );
  return sOut;
}

void SoundMenu::render()
{
  uint8_t a = 255;
  if( _state == FadeIn ) {

    a = getFadeInAlpha( _ticks, FadeTicks );
  }
  else if( _state == FadeOut ) {

    a = getFadeOutAlpha( _ticks, FadeTicks );
  }

  Menu::drawBox( BoxX, BoxY, BoxW / 8, BoxH / 8, Menu::ColorNormal, a );
    
  //const int FontW = 8;
  //const int FontW = 10;

  int x = ( ScreenW - 14 * FontW ) / 2;
  int y = BoxY + (ButtonH - FontH) / 2;
  
  g_font10->printBlended( x, y, a, "SOUND SETTINGS" );
  y += RowH;

  for( int i = 0; i < NUM_BUTTONS; ++i )
    _buttons[ i ].render( a );

  g_font10->printBlended( UpperX1, y, a, "SFX:        %s", g_options->isSFXEnabled() ? "ON" : "OFF" );
  y += RowH;

  g_font10->printBlended( UpperX1, y, a, "SFX VOLUME: %02d", g_options->getSFXVolume() );
  y += RowH;

  g_font10->printBlended( UpperX1, y, a, "BGM:        %s", g_options->isBGMEnabled() ? "ON" : "OFF" );
  y += RowH;

  g_font10->printBlended( UpperX1, y, a, "BGM VOLUME: %02d", g_options->getBGMVolume() );
  y += RowH;
  
  g_font10->printBlended( UpperX1, y, a, "BGM MODE:   %s", g_options->isBGMShuffle() ? "SHUFFLE" : "NORMAL" );
  y += RowH;

  const int FilenameX = (ScreenW - ( 25 * FontW )) / 2;

  std::string filename(strToUpper(g_bgmPlayer->getCurrentFilename()));
  if( filename.size() > 20 ) {

    filename += "        ";

    const int offset = _ticks / (200 / Period);

    char buffer[21];
    for( int i = 0; i < 20; ++i ) {
      
      buffer[i] = filename[ (i + offset) % filename.size() ];
    }
    buffer[20] = 0;
    
    g_font10->printBlended( FilenameX, y, a, "BGM: %s", buffer );
  }
  else {

    g_font10->printBlended( FilenameX, y, a, "BGM: %s", filename.c_str() );
  }

  y += RowH;
}

void SoundMenu::handleClick( int x, int y )
{
  if( _state != Active )
    return;

  for( int i = 0; i < NUM_BUTTONS; ++i ) {

    if( !_buttons[ i ].geometry.contains( x, y ) )
      continue;

    setCurrentButton(i);

    doButton(i);
    break;
  }
}

void SoundMenu::handleKey( int key )
{
  if( _state != Active )
    return;

  switch( key ) {

  case KeyLeft:
    if( _currentButton == B_SFX_TOGGLE_NEXT ||
	_currentButton == B_SFX_VOLUME_NEXT ||
	_currentButton == B_BGM_TOGGLE_NEXT ||
	_currentButton == B_BGM_VOLUME_NEXT ||
	_currentButton == B_BGM_MODE_NEXT ||
	_currentButton == B_BACK ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton - 1 );
    }
    break;

  case KeyRight:
    if( _currentButton == B_SFX_TOGGLE_PREV ||
	_currentButton == B_SFX_VOLUME_PREV ||
	_currentButton == B_BGM_TOGGLE_PREV ||
	_currentButton == B_BGM_VOLUME_PREV ||
	_currentButton == B_BGM_MODE_PREV ||
	_currentButton == B_BGM_SKIP ) {
      
      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton + 1 );
    }
    break;

  case KeyUp:
    if( _currentButton >= B_SFX_VOLUME_PREV ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton - 2 );
    }
    break;

  case KeyDown:
    if( _currentButton <= B_BGM_MODE_NEXT ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton + 2 );
    }
    break;
    
  case KeyOk:
    doButton(_currentButton);
    break;

  case KeyMenu:
    g_stateManager.popState();
    break;
  }
}

void SoundMenu::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    _currentButton = B_BGM_SKIP;
    for( int i = 0; i < NUM_BUTTONS; ++i ) {
      
      _buttons[ i ].setFocus( i == _currentButton );
      _buttons[ i ].setState( Button::Idle );
    }
    
    setState(FadeIn);
    //setTransitionState(TransitionInDone);
  }
  else {

    setState(FadeOut);
    //setTransitionState(TransitionOutDone);
  }
}

void SoundMenu::doButton(int button)
{
  switch( button ) {
      
  case B_SFX_TOGGLE_PREV:
  case B_SFX_TOGGLE_NEXT:
    g_options->setSFXEnabled( !g_options->isSFXEnabled() );
    _buttons[ button ].setState( Button::Ok );
    break;

  case B_SFX_VOLUME_PREV:
    if( g_options->getSFXVolume() > 0 ) {

      g_options->setSFXVolume( g_options->getSFXVolume() - 1 );
      setVolume();
      _buttons[ button ].setState( Button::Ok );
    }
    else {

      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_SFX_VOLUME_NEXT:
    if( g_options->getSFXVolume() < 10 ) {

      g_options->setSFXVolume( g_options->getSFXVolume() + 1 );
      setVolume();
      _buttons[ button ].setState( Button::Ok );
    }
    else {

      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_BGM_TOGGLE_PREV:
  case B_BGM_TOGGLE_NEXT:
    g_options->setBGMEnabled( !g_options->isBGMEnabled() );

    if( g_options->isBGMEnabled() ) {

      g_bgmPlayer->play();
    }
    else {

      g_bgmPlayer->stop();
    }

    _buttons[ button ].setState( Button::Ok );
    break;

  case B_BGM_VOLUME_PREV:
    if( g_options->getBGMVolume() > 0 ) {

      g_options->setBGMVolume( g_options->getBGMVolume() - 1 );
      setVolume();
      _buttons[ button ].setState( Button::Ok );
    }
    else {

      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_BGM_VOLUME_NEXT:
    if( g_options->getBGMVolume() < 10 ) {

      g_options->setBGMVolume( g_options->getBGMVolume() + 1 );
      setVolume();
      _buttons[ button ].setState( Button::Ok );
    }
    else {

      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_BGM_MODE_PREV:
  case B_BGM_MODE_NEXT:
    g_options->setBGMShuffle( !g_options->isBGMShuffle() );
    _buttons[ button ].setState( Button::Ok );
    break;

  case B_BGM_SKIP:
    g_bgmPlayer->next();
    _buttons[ button ].setState( Button::Ok );
    break;

  case B_BACK:
    _buttons[ button ].setState( Button::Ok );
    g_stateManager.popState();
    break;
  }
}

void SoundMenu::setCurrentButton(int button)
{
  _buttons[_currentButton].setFocus( false );
  _currentButton = button;
  _buttons[_currentButton].setFocus( true );
}

void SoundMenu::setState(State state)
{
  _state = state;
  _ticks = 0;
}
