#include "game.h"
#include "globals.h"
#include "options.h"
#include "state.h"

AbstractSystem* g_system(0);

const char* GameTitle = "RAINBOW PLAINS";

PropertiesFile* g_propertiesFile = 0;

Options* g_options = 0;

Tme::SpriteFont* g_font8 = 0;
Tme::SpriteFont* g_font10 = 0;

unsigned g_buttons = 0;
unsigned g_buttonsPrev = 0;

unsigned g_keys = 0;
unsigned g_keysPrev = 0;

bool flagKeyPressed()
{
  return g_keys & ( 1 << KeyFlag );
}

bool g_audio = true;

Mix_Chunk* g_sfx[ NumSFX ];

const char* g_sfxFiles[ NumSFX ] = {

  "../data/zap11.wav",
  "../data/bling01.wav",
  "../data/explosion01.wav",
  "../data/blop01.wav",
  "../data/won01.wav",
};

void playSFX( SFXType type )
{
  if( !g_audio || !g_options->isSFXEnabled() )
    return;

  if( type >= NumSFX )
    return;

  Mix_Chunk* chunk = g_sfx[ type ];
  if( !chunk )
    return;

  Mix_PlayChannel( -1, chunk, 0 );
}

int g_volumeControlTicks = 0;

static const int VolumeControlTicks = 1500 / Period;

void setVolume()
{
  if( !g_audio )
    return;
  
  float s = g_options->getBGMVolume() * 0.1f;
  s *= s;
  const int bgmVolume = s * MIX_MAX_VOLUME;

  s = g_options->getSFXVolume() * 0.1f;
  s *= s;
  const int sfxVolume = s * MIX_MAX_VOLUME;
  
  Mix_Volume( -1, sfxVolume );
  Mix_VolumeMusic( bgmVolume );
}

void volumeUp()
{
  g_options->setBGMVolume( g_options->getBGMVolume() + 1 );
  g_options->setSFXVolume( g_options->getSFXVolume() + 1 );
  setVolume();
  g_volumeControlTicks = VolumeControlTicks;
}

void volumeDown()
{
  g_options->setBGMVolume( g_options->getBGMVolume() - 1 );
  g_options->setSFXVolume( g_options->getSFXVolume() - 1 );
  setVolume();
  g_volumeControlTicks = VolumeControlTicks;
}

BGMPlayer* g_bgmPlayer = 0;

float clamp( float x, float a, float b )
{
  if( x < a ) return a;
  else if( x > b ) return b;
  else return x;
}

Uint8 getFadeInAlpha( float x, float n )
{
  float xn = clamp( x / n, 0.0f, 1.0f );
  return 255.0f * xn * xn * ( 3.0f - 2.0f * xn );
}

Uint8 getFadeOutAlpha( float x, float n )
{
  return 255 - getFadeInAlpha( x, n );
}

Game* g_game( 0 );
Title* g_title( 0 );

bool g_touchMode( false );

Highscores* g_highscores( 0 );

// static unsigned getMSB( unsigned x )
// {
//   // http://aggregate.org/MAGIC/#Most%20Significant%201%20Bit
//   x |= ( x >> 1 );
//   x |= ( x >> 2 );
//   x |= ( x >> 4 );
//   x |= ( x >> 8 );
//   x |= ( x >> 16 );
//   return ( x & ~( x >> 1 ) );
// }

// Blink durations in ms -> ticks

#define MAX( a, b ) ( ( a > b ) ? a : b )

static const unsigned BlinkTicks[ 3 ] = {
  
  MAX( (  4 * FPS ) / 60, 1 ),
  MAX( ( 32 * FPS ) / 60, 2 ),
  MAX( ( 64 * FPS ) / 60, 3 ),
};

bool getBlink( BlinkSpeed speed, int ticks )
{
  return ( ticks % ( BlinkTicks[ speed ] * 2 ) ) <  BlinkTicks[ speed ];
}

STM::StateManager g_stateManager;

int getMaxBombs(int size)
{
  return (getMapWidth(size) * getMapHeight(size)) / 4;
}

int getNumBombsDefault(int size)
{
  return (getMapWidth(size) * getMapHeight(size)) / 6;
}


int getMapWidth( int size )
{
  switch( size ) {

  case Game::Small:
    return ScreenW / TileSizeSmallMap;
  case Game::Medium:
    return ScreenW / TileSizeMediumMap;
  case Game::Large:
    return ScreenW / TileSizeLargeMap;
  }

  return 0;
}

int getMapHeight( int size )
{
  switch( size ) {

  case Game::Small:
    return (ScreenH - MenuBarH) / TileSizeSmallMap;
  case Game::Medium:
    return (ScreenH - MenuBarH) / TileSizeMediumMap;
  case Game::Large:
    return (ScreenH - MenuBarH) / TileSizeLargeMap;
  }

  return 0;
}

bool isValidMapSize( int w, int h )
{
  return 
    ( w == getMapWidth(Game::Small) && h == getMapHeight(Game::Small) ) ||
    ( w == getMapWidth(Game::Medium) && h == getMapHeight(Game::Medium) ) ||
    ( w == getMapWidth(Game::Large) && h == getMapHeight(Game::Large) );
}
