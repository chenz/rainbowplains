#include "game.h"
#include "globals.h"
#include "highscores.h"
#include "sprites.h"
#include "system.h"
#include "title.h"
#include "wiz.h"

#include <tmepp/tmepp.h>

#include <stdlib.h>
#include <string.h>

//static const unsigned MaskCount = 0x0F;
//static const unsigned MaskBomb = 0x10;
//static const unsigned MaskCover = 0x20;

//static const unsigned UncoverCount = 250 / Period;
static const unsigned UncoverCount = 120 / Period;
const int WipeW = 3;

GameKey Game::getDefaultGameKey(int size)
{
  return GameKey( getMapWidth( size ), getMapHeight( size ), getNumBombsDefault( size ) );
}

Game::Game( STM::StateManager* manager )
  : ::State( manager ),
    _map( 0 ), 
    _keyRepeater( this, 300 / Period, 100 / Period ),
    _flagAnimTicks( 0 ), 
    _flagAnimOffset( 0 ),
    _explodedTile(0)
{
  assert( !g_game );
  g_game = this;
  
  _keyboard = new Keyboard( &g_stateManager );
  _highscoreScreen = new HighscoreScreen( &g_stateManager );

  std::vector< std::string > buttons;
  buttons.push_back( "CONTINUE GAME" );
  buttons.push_back( "RESTART" );
  buttons.push_back( "BACK TO MENU" );

  const int MenuW = Menu::roundSize(140);
  const int MenuH = 3 * 24 + 2 * 6;

  _menu = new Menu( &g_stateManager );
  _menu->addButtons( buttons, 
		     ( ScreenW - MenuW ) / 2, ( ScreenH - MenuH ) / 2,
		     MenuW, 24, 
		     6 );

  for( unsigned i = 0; i < buttons.size(); ++i ) {

    _menu->buttons[ i ].setNeighbour( Button::Bottom, 
				      &( _menu->buttons[ ( i + 1 ) % buttons.size() ] ) );
  }

  _menu->listener = this;
  _menu->allowBack = true;

  _keyboard->setListener( this );
}

Game::~Game()
{
  if( _map )
    delete [] _map;

  delete _menu;
}

void Game::setGameKey( const GameKey& key )
{
  _gameKey = key;

  switch( key.getGameSize() ) {
    
  case Large:
    _tileW = TileSizeLargeMap;
    _tileH = TileSizeLargeMap;
    _spriteBaseCovered = SPRITE_TILE16_COVERED_0;
    _spriteBaseUncovered = SPRITE_TILE16_UNCOVERED_0;
    break;
  case Medium:
    _tileW = TileSizeMediumMap;
    _tileH = TileSizeMediumMap;
    _spriteBaseCovered = SPRITE_TILE24_COVERED_0;
    _spriteBaseUncovered = SPRITE_TILE24_UNCOVERED_0;
    break;
  case Small:
    _tileW = TileSizeSmallMap;
    _tileH = TileSizeSmallMap;
    _spriteBaseCovered = SPRITE_TILE32_COVERED_0;
    _spriteBaseUncovered = SPRITE_TILE32_UNCOVERED_0;
    break;
  }

  _mapW = ScreenW / _tileW;
  _mapH = ( ScreenH - MenuBarH ) / _tileH;
  _numTiles = _mapW * _mapH;

  if( _map )
    delete [] _map;

  _map = new Tile[ _numTiles ];
  _numBombs = (key.numBombs > 0) ? key.numBombs : getNumBombsDefault(key.getGameSize());
  _numFlagged = 0;

  //createMap();
}

void Game::update()
{
  //if( StateManager::instance()->topState() != this )
  //  return;

  if( !isTopState() )
    return; // FIXME
  
  _ticks++;
  _keyRepeater.update();
  
  const unsigned FadeTicks = 500 / Period;
  const unsigned WaitTicks = 2000 / Period;
  const unsigned LostWonWaitTicks = WaitTicks - FadeTicks;

  const unsigned FlagAnimTicks = 250 / Period;
  
  _flagAnimTicks++;
  if( _flagAnimTicks >= FlagAnimTicks ) {

    _flagAnimTicks = 0;
    _flagAnimOffset = ( _flagAnimOffset + 1 ) % 6;
  }
  
  switch( _state ) {
    
  case FadeIn:
    _alpha = getFadeInAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {

      setTransitionState( TransitionInDone );
      setState( Waiting );
    }
    break;
    
  case Start:
    {
      _clock += Period;    
    
      updateCover();
  
      if( isWon() )
	setState( Won );
    }
    break;

  case Won:
    updateCover();

    if( _ticks >= LostWonWaitTicks )
      _lostWonAlpha = getFadeOutAlpha( _ticks - LostWonWaitTicks, FadeTicks );

    if( _ticks >= WaitTicks ) {

	int pos = g_highscores->getPosition( _gameKey, _clock );
	if( pos >= 0 ) {
	  
	  _highscoreScreen->setGameKey( _gameKey );
	  _keyboard->setTitle( "ENTER YOUR NAME!" );
	  _keyboard->setString( g_highscores->lastName );
	  //StateManager::instance()->pushState( _keyboard );
	  //StateManager::instance()->setNextState( _highscoreScreen );
	  _keyboard->nextState = _highscoreScreen; // FIXME
	  g_stateManager.pushState( _keyboard );
	  
	  setState( Highscores );
      }
      else {

	setState( Waiting );
      }
    }
    break;

  case Lost:
    updateCover();

    if( _ticks >= LostWonWaitTicks )
      _lostWonAlpha = getFadeOutAlpha( _ticks - LostWonWaitTicks, FadeTicks );

    if( _ticks >= WaitTicks ) {

      setState( Waiting );
    }    
    break;
    
  case Waiting:
    break;
    
  case Wipe:
    {
      const unsigned WipeTicksPerTile = std::max( (unsigned) 1, 500 / ( Period * _mapW ) );
      //const unsigned WipeTicksPerTile = 1;
      if( _ticks >= WipeTicksPerTile ) {

	_ticks = 0;
	_wipePos++;
	
	for( int j = 0; j < (int) _mapH; ++j ) {
	  
	  for( int i = 0; i < ( _wipePos - (int) _mapH + j ) && i < (int) _mapW; ++i ) {
	    
	    _map[ ( j * _mapW ) + i ].covered = true;
	    _map[ ( j * _mapW ) + i ].flag = false;
	  }
	}
      }
      
      if( _wipePos >= (int)( _mapW * 2 + WipeW * 2 ) ) {
	
	setState( Start );
      }
    }
    break;

  case Highscores:
    setState( Waiting );
    break;

  case FadeOut:
    _alpha = getFadeOutAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {
      
      //StateManager::instance()->setNextState( g_title );
      //StateManager::instance()->popState();      

      setTransitionState( TransitionOutDone );
    }
  }
}

void Game::renderWipe( const int ox, const int oy )
{
  int x0 = ox + ( _wipePos - 1 ) * _tileW;
 
  int y = oy + ( _mapH - 1 ) * _tileH;
  for( int j = _mapH - 1; j >= 0; --j ) {
    
    int x = x0;
    for( int i = 0; i < WipeW; ++i ) {
      
      Tme::Video::drawSolidRect( Tme::FloatRect( x, y, _tileW, _tileH ),
				 255, 255, 255, 255 - i * ( 150 / WipeW ) );
      x -= _tileW;
    }
    
    x0 -= _tileW;
    y -= _tileH;
  }
}

void Game::render()
{
  //bool inMenu = ( StateManager::instance()->topState() == _menu );
  const bool inMenu = g_stateManager.getTopState() == _menu;

  const bool isLost = _state == Lost || ( _state == Waiting && _prevState == Lost );
  const bool isWon = _state == Won || ( _state == Waiting && ( _prevState == Won || _prevState == Highscores ) );
  const bool isPostGame = isLost || isWon;
  
  // Menu bar...
  if( _state != FadeIn &&
      _state != FadeOut ) {
    
    const int seconds = ( _clock / 1000 ) % 60;
    const int minutes = _clock / 60000;
    
    char buffer[ 6 ];
    snprintf( buffer, sizeof( buffer ), "%02d:%02d", minutes, seconds );
    g_font8->print( 4, 4, buffer );

    snprintf( buffer, sizeof( buffer ), "% 4d", _numBombs - _numFlagged );
    g_font8->print( ScreenW - 4 - 4 * 8, 4, buffer );
    
    if( g_touchMode ) {
     
      g_font8->print( ( ScreenW - 8 * 4 ) / 2, 4, "MENU" );

      if( flagKeyPressed() ) {

	Tme::Video::blitSprite( SPRITE_FLAG_0 + _flagAnimOffset, 
				ScreenW - 11 - 4 - 4 - 4 * 8, 2 );
      }
    }
  }
  
  // Map...
  const int offsetX = ( ScreenW - _mapW * _tileW ) / 2;
  const int offsetY = MenuBarH + ( ScreenH - MenuBarH - _mapH * _tileH ) / 2;
  const int offsetCount = ( _tileW - 8 ) / 2;

  
  
  int k = 0;
  float y = offsetY;
  for( unsigned j = 0; j < _mapH; ++j ) {

    float x = offsetX;
    for( unsigned i = 0; i < _mapW; ++i ) {
      
      Tile* t = &( _map[ k++ ] );
      
      if( inMenu ) {

	// "hide" the map while menu is shown...
	Tme::Video::blitSprite( _spriteBaseUncovered + t->color, x, y );
      }
      else {
	
	if( t->covered || t->bomb /*|| t->flag*/ ) {
	
	  Tme::Video::blitSprite( _spriteBaseCovered + t->color, x, y );
	}

	if( !t->covered ) {
	  
	  if( t->uncoverCount > 0 ) {
	
	    // Uncovering in progress...
	    Tme::Video::drawSolidRect( Tme::FloatRect( x, y, _tileW, _tileH ),
				       0xFF, 0xFF, 0xFF, 0xFF );
	  }
	  else {

	    // Fully uncovered...

	    if( !t->bomb ) {
	    
	      Tme::Video::blitSprite( _spriteBaseUncovered + t->color, x, y );
	      
	      if( isPostGame && t->flag && getBlink( BlinkMedium, _ticks ) ) {
		
		Tme::Video::blitSprite( SPRITE_FLAG_0 + _flagAnimOffset,
					x + offsetCount - 2,
					y + offsetCount - 2 );
	      }
	      else if( t->count ) {
		
		Tme::Video::blitSprite( SPRITE_1 + 1 - t->count, x + offsetCount, y + offsetCount );
	      }
	    }
	    else if( t == _explodedTile ) {
	      
	      Tme::Video::blitSprite( SPRITE_BOMB_RED, x + offsetCount, y + offsetCount );
	    }
	    else {

	      if( isPostGame && t->flag && getBlink( BlinkMedium, _ticks ) ) {

		Tme::Video::blitSprite( SPRITE_FLAG_0 + _flagAnimOffset,
					x + offsetCount - 2,
					y + offsetCount - 2 );
	      }
	      else {
		
		Tme::Video::blitSprite( SPRITE_BOMB, x + offsetCount, y + offsetCount );
	      }
	    }
	  }
	}
	else if( t->flag ) {

	  Tme::Video::blitSprite( SPRITE_FLAG_0 + _flagAnimOffset,
				  x + offsetCount - 2,
				  y + offsetCount - 2 );
	}
      }
      
      x += _tileW;
    }
    
    y += _tileH;
  }

  // Cursor...
  if( !g_touchMode && /*StateManager::instance()->topState() == this*/ isTopState() &&
      ( _state == Start ) ) {

    int offset = 0;
    switch( _gameKey.getGameSize() ) {

    case Medium:
      offset = SPRITE_TILEFRAME24_A - SPRITE_TILEFRAME16_A;
      break;
    case Small:
      offset = SPRITE_TILEFRAME32_A - SPRITE_TILEFRAME16_A;
      break;
    default:
      break;
    }
    
    Tme::Video::blitSprite( SPRITE_TILEFRAME16_A + offset, 
			    offsetX + (int)( _cursorX * _tileW ) - 4, 
			    offsetY + (int)( _cursorY * _tileH ) - 4 );
  }

  const int WinW = 129;
  const int LoseW = 146;

  if( isWon ) {
    
    Tme::Video::blitSprite( SPRITE_WIN, ( ScreenW - WinW ) / 2, 80, _lostWonAlpha );
  }
  else if( isLost ) {
    
    Tme::Video::blitSprite( SPRITE_LOSE, ( ScreenW - LoseW ) / 2, 80, _lostWonAlpha );
  }
  
  const int FontW = 10;

  if( _state == Waiting ) {

    if( g_touchMode && getBlink( BlinkSlow, _ticks ) ) {
      
      //g_font8->print( ( ScreenW - 8 * 14 ) / 2, 220, "CLICK TO START" );
      g_font10->print( ( ScreenW - FontW * 14 ) / 2, 220, "CLICK TO START" );
    }
    if( !g_touchMode && getBlink( BlinkSlow, _ticks ) ) {

      //g_font8->print( ( ScreenW - 8 * 20 ) / 2, 220, "PRESS BUTTON TO START" );
      g_font10->print( ( ScreenW - FontW * 20 ) / 2, 220, "PRESS BUTTON TO START" );
    }
  }
  
  if( _state == Wipe && isTopState() ) {

    renderWipe( offsetX, offsetY );
  }

  if( _state == FadeIn ||
      _state == FadeOut ) {

    Menu::drawFade( 0, 0, 0, 255 - _alpha );
  }
}

void Game::handleClick( int x, int y )
{
  if( y < MenuBarH &&
      _state != FadeOut ) {
    
    //StateManager::instance()->pushState( _menu );
    g_stateManager.pushState( _menu );
    return;
  }
  
  switch( _state ) {

  case FadeIn:
  case Won:
  case Lost:
  case Wipe:
  case Highscores:
  case FadeOut:
    break;

  case Start:
    {
      const int offsetX = ( ScreenW - _mapW * _tileW ) / 2;
      const int offsetY = MenuBarH + ( ScreenH - MenuBarH - _mapH * _tileH ) / 2;
      
      x -= offsetX;
      y -= offsetY;
      
      int i = x / _tileW;
      int j = y / _tileW;
      
      if( i < 0 || i >= (int) _mapW ||
	  j < 0 || j >= (int) _mapH )
	return;
      
      Tile* t = &( _map[ i + j * _mapW ] );
      
      handleTileClick( t, flagKeyPressed() );
    }
    break;

  case Waiting:
    if( _prevState != FadeIn )
      setState( Wipe );
    else
      setState( Start );
    break;
  }
}

void Game::handleKey( int key )
{
  if( g_touchMode )
    return;

  _keyRepeater.handleKey( key );

  if( key == KeyMenu &&
      _state != FadeOut ) {
    
    //StateManager::instance()->pushState( _menu );
    g_stateManager.pushState( _menu );
    return;
  }
  
  // Testing...
  if( key == KeyCheat ) {
  
    uncoverAll();
    //_clock = 5000;
    setState( Won );
    return;
  }

  switch( _state ) {

  case FadeIn:
  case Won:
  case Lost:
  case Wipe:
  case Highscores:
  case FadeOut:
    break;
    
  case Start:
    switch( key ) {
      
    case KeyUp:
      if( _cursorY )
	_cursorY--;
      break;
      
    case KeyDown:
      if( _cursorY + 1 < _mapH )
	_cursorY++;
      break;
      
    case KeyLeft:
      if( _cursorX )
	_cursorX--;
    break;
    
    case KeyRight:
      if( _cursorX + 1 < _mapW )
	_cursorX++;
      break;

    case KeyOk:
      handleTileClick( &( _map[ _cursorX + _cursorY * _mapW ] ), false );
      break;

    case KeyBack:
    case KeyFlag:
      handleTileClick( &( _map[ _cursorX + _cursorY * _mapW ] ), true );
      break;
    }
    break;

  case Waiting:
    if( _prevState != FadeIn )
      setState( Wipe );
    else
      setState( Start );
    break;
  }
}

void Game::handleTileClick( Tile* t, bool flag )
{
  if( !t->covered )
    return;
  
  if( flag ) {

    t->flag = !t->flag;
    
    _numFlagged += t->flag ? 1 : -1;

    playSFX( SFX_Uncover );
    return;
  }
  else if( t->flag ) {
    
    playSFX( SFX_Buzzer );
    return;
  }

  t->covered = false;
  
  _numCovered--;
  
  //printf( "%u / %u\n", numCovered, numBombs );
  
  if( t->bomb ) {
    
    _explodedTile = t;

    playSFX( SFX_Explosion1 );
    setState( Lost );
  }
  else if( t->count ) {
    
    t->uncoverCount = UncoverCount;
    playSFX( SFX_Uncover );
  }
  else {
    
    t->uncoverCount = UncoverCount;
    playSFX( SFX_UncoverBulk );
  }
}

void Game::setState( State state )
{
  _ticks = 0;
  
  _prevState = _state;
  _state = state;

  switch( state ) {

  case FadeIn:
    _alpha = 0;
    _numFlagged = 0;
    break;
  case Start:
    _clock = 0;
    _cursorX = _mapW / 2;
    _cursorY = _mapH / 2;
    createMap();
    uncoverOne();
    _numFlagged = 0;
    _explodedTile = 0;
    break;
  case Lost:
    _lostWonAlpha = 255;
    uncoverAll();
    break;
  case Won:
    _lostWonAlpha = 255;
    uncoverAll();
    playSFX( SFX_Won );
    break;
  case Highscores:
    break;
  case Waiting:
    break;
  case Wipe:
    _wipePos = 0;
    break;
  case FadeOut:
    _alpha = 1;
    break;
  }
}

// void Game::enter()
// {
//   _clock = 0;
//   createMap();
//   setState( FadeIn );
// }

void Game::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    if( getTransitionType() != TransitionChild ) {
      // Newly entered...
      _clock = 0;
      createMap();
      setState( FadeIn );
    }
    else {    

      // Out of menu: instantanious...
      setTransitionState( TransitionInDone );
    }
  }
  else if( getTransitionState() == TransitionOut ) {

    if( getTransitionType() != TransitionChild ) {

      // Leave game state...
      setState( FadeOut );
    }
    else {

      // Into menu: instantanious...
      setTransitionState( TransitionOutDone );
    }
  }
}

void Game::onMenuFinished( int index )
{
  switch( index ) {

  case 0:
    // Continue
    return;

  case 1:
    // Restart
    //setState(Waiting);
    setState(Wipe);
    return;

  case 2:
    // Back to title
    g_stateManager.swapTopState( g_title );
  }
}

void Game::onTextEntered( const std::string& text )
{
  g_highscores->setHighscore( _gameKey, text, _clock );
}

void Game::createMap()
{
  _numCovered = _numTiles;

  unsigned i;
  for( i = 0; i < _numTiles; ++i ) {
    
    _map[ i ].color = ( i % _mapW + i / _mapW ) % 8;
    _map[ i ].count = 0;
    _map[ i ].bomb = false;
    _map[ i ].covered = true;
    _map[ i ].flag = false;
    _map[ i ].uncoverCount = 0;
  }

  for( i = 0; i < _numBombs; ++i ) {
      
    unsigned x;
    unsigned y;

    for( ;; ) {
      x = rand() % _mapW;
      y = rand() % _mapH;

      if( !_map[ x + y * _mapW ].bomb )
	break;
    }

    _map[ x + y * _mapW ].bomb = true;
    incBombCount( x - 1, y - 1 );
    incBombCount(     x, y - 1 );
    incBombCount( x + 1, y - 1 );
    incBombCount( x - 1,     y );
    incBombCount( x + 1,     y );
    incBombCount( x - 1, y + 1 );
    incBombCount(     x, y + 1 );
    incBombCount( x + 1, y + 1 );
  }
}

void Game::incBombCount( int x, int y ) 
{
  if( x < 0 || x >= (int) _mapW ||
      y < 0 || y >= (int) _mapH ) {

    return;
  }

  int i = x + y * _mapW;
  if( _map[ i ].bomb )
    return;

  _map[ i ].count++;
}

bool Game::checkUncover( int i, int j ) const
{
  if( i < 0 || i >= (int) _mapW ||
      j < 0 || j >= (int) _mapH ) {

    return false;
  }

  const Tile* t = &( _map[ i + j * _mapW ] );
  if( t->covered ||
      t->count > 0 || 
      t->uncoverCount == (int) UncoverCount ) {
    
    return false;
  }
  
  return true;
}

bool Game::isWon() const
{
  return ( _state == Start || _state == Won ) && ( _numBombs == _numCovered ); 
}

void Game::uncoverAll()
{
  for( unsigned i = 0; i < _numTiles; ++i ) {
    
    if( _map[ i ].covered ) {
      
      _map[ i ].covered = false;
      _numCovered--;
      _map[ i ].uncoverCount = UncoverCount;
    }
  }
}

void Game::uncoverOne()
{
  unsigned i = rand() % _numTiles;

  bool foundEmpty = false;
  for( unsigned j = 0; j < _numTiles; ++j ) {
    
    if( !_map[i].count && !_map[i].bomb ) {
  
      foundEmpty = true;
      break;
    }

    i = (i + 1) % _numTiles;
  }

  if( !foundEmpty ) {

    for( unsigned j = 0; j < _numTiles; ++j ) {
      
      if( !_map[i].bomb ) {
	
	foundEmpty = true;
	break;
      }
      
      i = (i + 1) % _numTiles;
    }
  }

  if( !foundEmpty )
    return;

  playSFX( SFX_UncoverBulk );
  _map[ i ].covered = false;
  _numCovered--;
  _map[ i ].uncoverCount = UncoverCount;
}

void Game::updateCover()
{
  int k = 0;
  for( int j = 0; j < (int) _mapH; ++j ) {
    for( int i = 0; i < (int) _mapW; ++i ) {
      
      Tile* t = &( _map[ k++ ] );
      
      if( !t->covered || t->flag ) {
	
	continue;
      }
      
      if( checkUncover( i - 1, j - 1 ) ||
	  checkUncover(     i, j - 1 ) ||
	  checkUncover( i + 1, j - 1 ) ||
	  checkUncover( i - 1,     j ) ||
	  checkUncover( i + 1,     j ) ||
	  checkUncover( i - 1, j + 1 ) ||
	  checkUncover(     i, j + 1 ) ||
	  checkUncover( i + 1, j + 1 ) ) {
	
	t->covered = false;
	t->uncoverCount = UncoverCount;
	
	_numCovered--;
      }
    }
  }

  for( unsigned i = 0; i < _numTiles; ++i ) {
    
    if( _map[ i ].uncoverCount > 0 )
      _map[ i ].uncoverCount--;
  }
}

