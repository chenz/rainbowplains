#ifndef __SYSTEM_H__
#define __SYSTEM_H__

class AbstractSystem
{
public:
  virtual ~AbstractSystem() {}

  //! Map joystick button to "virtual" key
  virtual int mapJoyButton( unsigned button ) const = 0;
  //! Map keyboard to "virtual" key
  virtual int mapKey( unsigned key ) const = 0;

  //! Set screensaver status...
  virtual void enableScreensaver( bool on ) {}

  //! Render debug info...
  virtual void dbgShowStats() {}

  //! Map joystick button to "flag" key
  bool isFlagButton( unsigned button ) const;

  //! Map keyboard to "flag" key
  bool isFlagKey( unsigned key ) const;

  //! Check volume controls...
  void checkVolume();
};

#endif // __SYSTEM_H__
