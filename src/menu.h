#ifndef __MENU_H__
#define __MENU_H__

#include "button.h"
#include "state.h"

#include <tmepp/tmepp.h>

#include <string>
#include <vector>

struct MenuListener
{
  virtual ~MenuListener() {}
  virtual void onMenuFinished( int index ) = 0;
};

class Menu : public State
{
 public:

  enum BoxColor {
    
    ColorNormal,
    ColorRed,
    ColorGreen,
  };

  static unsigned roundSize( unsigned s );
  static void drawBox( const int x, const int y, const int cols, const int rows, BoxColor color, uint8_t alpha );
  static void drawFrame( const int x, const int y, const int cols, const int rows, uint8_t alpha );
  static void drawFade( const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a ); 

  Menu( STM::StateManager* manager /*const Tme::FloatRect& geometry, const std::vector< std::string >& buttonTexts*/ );

  unsigned addButtons( const std::vector< std::string >& texts,
		       int x, int y, 
		       int w, int h,
		       int spacing );

  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  //void enter();

  void onTransition( STM::State* prevState );

  int getButtonIndex( const Button* b ) const;

  uint8_t alpha;
  MenuListener* listener;

  bool allowBack;

  std::vector< Button > buttons;

 private:

  enum State {

    FadeIn,
    Idle,
    FadeOut
  };

  State _state;

  void setState( State state );

  int ticks;
  int buttonIndex;
  int clickedIndex;
};

#endif // __MENU_H__
