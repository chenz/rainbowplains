#include "globals.h"
#include "state.h"
#include "system.h"

#include <tmepp/tmepp.h>

#include <SDL.h>

#include <cassert>

State::State( STM::StateManager* manager ) 
  : STM::State( manager ),
    nextState( 0 )
{
}

bool State::onSDLEvent( SDL_Event* event )
{
  switch( event->type ) {
    
  case SDL_MOUSEBUTTONDOWN:
    g_touchMode = true;
    handleClick( event->button.x / SCREEN_S, event->button.y / SCREEN_S );
    break;
    
  case SDL_JOYBUTTONDOWN:
    {
      if( g_touchMode && !g_system->isFlagButton( event->jbutton.button ) )
	g_touchMode = false;
      
      g_buttons |= ( 1 << event->jbutton.button );
      int key = g_system->mapJoyButton( event->jbutton.button );
      if( key >= 0 ) {
	
	g_keys |= ( 1 << key );
	if( !g_touchMode )
	  handleKey( key );
      }
    }
    break;
    
  case SDL_JOYBUTTONUP:
    {
      g_buttons &= ~( 1 << event->jbutton.button );
      int key = g_system->mapJoyButton( event->jbutton.button );
      if( key >= 0 ) {
	
	g_keys &= ~( 1 << key );
      }
    }
    break;

  case SDL_KEYDOWN:
    {
      if( g_touchMode && !g_system->isFlagKey( event->key.keysym.sym ) )
	g_touchMode = false;
      
      int key = g_system->mapKey( event->key.keysym.sym );
      if( key >= 0 ) {
	
	g_keys |= ( 1 << key );
	if( !g_touchMode )
	  handleKey( key );
      }
    }
    break;
    
  case SDL_KEYUP:
    {
      int key = g_system->mapKey( event->key.keysym.sym );
      if( key >= 0 ) {
	
	g_keys &= ~( 1 << key );
      }
    }
    break;
    
  case SDL_QUIT:
    return false; // Ignore event so that it handled by main loop...
    break;
  }

  return true;
}

// StateManager* StateManager::instance()
// {
//   static StateManager sm;
//   return &sm;
// }

// State* StateManager::topState()
// {
//   //if( _stack.size() )
//   //  return _stack.back();
//   //
//   //return 0;

//   if( _stackIndex < 0 ) 
//     return 0;
  
//   return _stack[ _stackIndex ];
// }

// void StateManager::pushState( State* state )
// {
//   assert( _stackIndex < 15 );
//   _stack[ ++_stackIndex ] = state;
//   state->enter();
    
//   //_stack.push_back( state );
//   //tmeDebugMsg( "%u", _stack.size() );
// }

// void StateManager::popState()
// {
//   assert( _stackIndex >= 0 );

//   State* tmp = _stack[ _stackIndex ];
//   _stack[ _stackIndex-- ] = 0;
//   tmp->leave();

//   if( _nextState ) {

//     tmp = _nextState;
//     _nextState = 0;
//     pushState( tmp );
//   }

//   //_stack.pop_back();
//   //tmeDebugMsg( "%u", _stack.size() );
// }

// void StateManager::setNextState( State* state )
// {
//   _nextState = state;
// }

// void StateManager::update()
// {
//   //for( unsigned i = 0; i < _stack.size(); ++i )
//   //  _stack[ i ]->update();

//   for( int i = 0; i <= _stackIndex; ++i )
//     _stack[ i ]->update();
// }

// void StateManager::render()
// {
//   //for( unsigned i = 0; i < _stack.size(); ++i )
//   //  _stack[ i ]->render();
//   for( int i = 0; i <= _stackIndex; ++i )
//     _stack[ i ]->render();
// }

// StateManager::StateManager()
//   : _stackIndex( -1 ), _nextState( 0 )
// {
// }
