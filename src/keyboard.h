#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include "keyrepeater.h"
#include "menu.h"
#include "state.h"

#include <tmepp/tmepp.h>

#include <string>

class KeyboardListener
{
 public:
  virtual void onTextEntered( const std::string& text ) = 0;
};

class Keyboard;

class KeyButton : public Button
{
 public:

  enum Function {
    
    Character,
    Space,
    Backspace,
    Left,
    Right,
    Enter
  };
  
  KeyButton( const Tme::FloatRect& geometry, 
	     char character );
  KeyButton( const Tme::FloatRect& geometry, 
	     const std::string& text, Function function );
  
  char character;
  Function function;
};

class Keyboard : public State
{
 public:
  Keyboard( STM::StateManager* manager );
  ~Keyboard();

  void setListener( KeyboardListener* listener );
  void setTitle( const std::string& title );
  void setString( const std::string& string );
  std::string getString() const;

  // State API...
  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  //void enter();
  
  void onTransition( STM::State* prevState );

 private:

  enum Dir {
    
    Up,
    Down,
    Left,
    Right
  };

  enum State {

    FadeIn,
    Idle,
    FadeOut,
  };

  void setState( State state );

  int getNextIndex( const int index, const Dir dir );
  void handleKeyButton( KeyButton* button );

  std::vector< KeyButton > _keys;

  KeyboardListener* _listener;
  std::string _title;

  static const int MaxLength = 16;
  char _string[ MaxLength + 1 ];
  int _cursor;

  int _currentButton;

  unsigned _ticks;

  KeyRepeater _keyRepeater;

  State _state;
  uint8_t _alpha;
};

#endif // __KEYBOARD_H__
