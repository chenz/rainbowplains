#include "game.h"
#include "gamekey.h"

GameKey GameKey::fromUnsigned( unsigned key )
{
  return GameKey( (key / 0x1000000),
		  (key /   0x10000) & 0xFF,
		  (key            ) & 0xFFFF );
}

GameKey::GameKey( unsigned w, unsigned h, unsigned numBombs )
  : width(w)
  , height(h)
  , numBombs(numBombs)
{
}

GameKey::GameKey( const GameKey& that )
  : width(that.width)
  , height(that.height)
  , numBombs(that.numBombs)
{
}

unsigned GameKey::toUnsigned() const
{
  return width * 0x1000000 + height * 0x10000 + numBombs;
}

bool GameKey::operator<( const GameKey& that ) const
{
  return toUnsigned() < that.toUnsigned();
}

bool GameKey::operator==( const GameKey& that ) const
{
  return toUnsigned() == that.toUnsigned();
}

bool GameKey::operator!=( const GameKey& that ) const
{
  return toUnsigned() != that.toUnsigned();
}

int GameKey::getGameSize() const
{
  if( (int) width == getMapWidth(Game::Small) && (int) height == getMapHeight(Game::Small) ) {

    return Game::Small;
  }
  else if( (int) width == getMapWidth(Game::Medium) && (int) height == getMapHeight(Game::Medium) ) {

    return Game::Medium;
  }
  else if( (int) width == getMapWidth(Game::Large) && (int) height == getMapHeight(Game::Large) ) {

    return Game::Large;
  }
  else {
    
    return -1;
  }
}
