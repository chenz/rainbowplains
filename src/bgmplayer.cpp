#include "bgmplayer.h"
#include "globals.h"
#include "options.h"

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <algorithm>

static std::string joinPath( const char* p1, const char* p2 )
{
  char buffer[1024];
  snprintf( buffer, sizeof(buffer), "%s/%s", p1, p2 );
  return std::string( buffer );
}

BGMPlayer::BGMPlayer()
  : _currentFile( -1 ), _currentMusic( 0 )
{
}

BGMPlayer::~BGMPlayer()
{
  unload();
}

bool BGMPlayer::init()
{
  for( std::list<std::string>::const_iterator it = g_options->getBGMPath().begin();
       it != g_options->getBGMPath().end(); ++it ) {

    scanDir( (*it).c_str() );    
  }

  return _files.size() > 0;
}

void BGMPlayer::play()
{
  if( _currentFile < 0 ) {

    next(); // Calls "play"...
    return;
  }

  if( !g_audio || !g_options->isBGMEnabled() )
    return;

  if( !_currentMusic )
    load();

  fprintf( stderr, "BGMPlayer::play: '%s'\n", _files[ _currentFile ].c_str() );

  _state = On;
  Mix_PlayMusic( _currentMusic, 1 );
}

void BGMPlayer::stop()
{
  _state = Off;
  unload();
}

void BGMPlayer::next()
{
  if( !_files.size() )
    return;

  if( _currentFile < 0  || g_options->isBGMShuffle() ) {

    _currentFile = rand() % _files.size();
  }
  else {
    
    _currentFile = ( _currentFile + 1 ) % _files.size();
  }

  load();
  play();
}

void BGMPlayer::update()
{
  if( g_audio && g_options->isBGMEnabled() && _files.size() && !Mix_PlayingMusic() ) {

    next();
    //play();
  }
}

std::string BGMPlayer::getCurrentFilename() const
{
  if( _currentFile < 0 || _currentFile >= (int) _files.size() )
    return "";

  const std::string& filename = _files[_currentFile];
  int pos = filename.rfind("/");
  if( pos == (int) filename.size() )
    return filename;

  return std::string(filename, pos + 1);
}

void BGMPlayer::scanDir( const char* dirName )
{
  std::vector< std::string > files;

  DIR* dp = opendir( dirName );
  if( !dp ) {
    
    perror( "opendir" );
    return;
  }

  const char* extensions[] = { ".it", ".mod", ".ogg", ".xm", 0 };
  
  struct dirent* de;
  while( ( de = readdir( dp ) ) ) {

    std::string fileName = joinPath( dirName, de->d_name );

    struct stat st;
    if( stat( fileName.c_str(), &st ) ) {

      perror( "stat" );
      continue;
    }

    if( S_ISDIR( st.st_mode ) ) {

      if( de->d_name[0] != '.' ) {

	scanDir( joinPath( dirName, de->d_name ).c_str() );
      }
    }
    else if( S_ISREG( st.st_mode ) ) {

      char* dot = strrchr( de->d_name, '.' );
      if( dot ) {

	for( int i = 0; extensions[i]; ++i ) {
	  
	  if( !strcasecmp( extensions[i], dot ) ) {
	    
	    files.push_back( de->d_name );
	    break;
	  }
	}
      }
    }
  }
  
  closedir( dp );
  
  std::sort( files.begin(), files.end() );
  for( unsigned i = 0; i < files.size(); ++i ) {
    
    _files.push_back( joinPath( dirName, files[i].c_str() ) );
    fprintf( stderr, "BGM: %s\n", _files.back().c_str() );
  }
}

void BGMPlayer::load()
{
  if( _currentMusic )
    unload();

  _currentMusic = Mix_LoadMUS( _files[ _currentFile ].c_str() );
  if( !_currentMusic ) {

    // ERROR
  }
}

void BGMPlayer::unload()
{
  if( _currentMusic ) {

    Mix_FreeMusic( _currentMusic );
    _currentMusic = 0;
  }
}
