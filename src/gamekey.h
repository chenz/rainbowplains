#ifndef _GAMEKEY_H_
#define _GAMEKEY_H_

class GameKey
{
public:

  static GameKey fromUnsigned( unsigned key );

  GameKey( unsigned w=0, unsigned h=0, unsigned numBombs=0 );
  GameKey( const GameKey& that );

  unsigned toUnsigned() const;
  
  bool operator<( const GameKey& that ) const;
  bool operator==( const GameKey& that ) const;
  bool operator!=( const GameKey& that ) const;

  int getGameSize() const;

  unsigned width;
  unsigned height;
  unsigned numBombs;
};

#endif /* _GAMEKEY_H_ */
