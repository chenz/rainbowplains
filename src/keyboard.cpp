#include "globals.h"
#include "keyboard.h"

KeyButton::KeyButton( const Tme::FloatRect& geometry, char character )
  : Button( geometry, std::string() ), character( character ), function( Character )
{
  char buffer[ 2 ];
  snprintf( buffer, sizeof( buffer ), "%c", character );
  setText( buffer );
}

KeyButton::KeyButton( const Tme::FloatRect& geometry, const std::string& text, Function function )
  : Button( geometry, text ), character( 0 ), function( function )
{
}

static const int Spacing = 4;
static const int TitleBoxH = 16;
static const int KeySize = 24;
static const int KeyAreaY = TitleBoxH + KeySize + Spacing * 4;

Keyboard::Keyboard( STM::StateManager* manager )
  : ::State( manager ),
    _keyRepeater( this, 300 / Period, 100 / Period )
{
  _listener = 0;
  _string[ 0 ] = 0;
  _cursor = 0;
  _currentButton = 0;

  const int NumRows = 5;
  const char* rows[ NumRows ] = {  
    "0123456789",
    "ABCDEFGHIJK",
    "LMNOPQRSTU",
    "VWXYZ!#()+,",
    "-./:<=>?",
  };
  
  int y = KeyAreaY;
  for( int j = 0; j < NumRows; ++j ) {

    int n = strlen( rows[ j ] );
    int x = ( ( (int) ScreenW ) - n * KeySize - ( n - 1 ) * Spacing ) / 2;
    
    for( int i = 0; i < n; ++i ) {
      
      _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize, KeySize ), 
				  rows[ j ][ i ] ) );
      
      x += KeySize + Spacing;
    }

    y += KeySize + Spacing;
  }

  //Space,
  //Backspace,
  //Left,
  //Right,
  //Enter

  int w = 
    KeySize * 2 + Spacing +
    KeySize * 2 + Spacing +
    KeySize * 3 + Spacing +
    KeySize * 2 + Spacing +
    KeySize * 2;
  
  int x = ( ScreenW - w ) / 2;
  
  y += Spacing;
  
  _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize * 2, KeySize ),
			      "<<", KeyButton::Left ) );
  x += 2 * KeySize + Spacing;
  
  _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize * 2, KeySize ),
			      ">>", KeyButton::Right ) );
  x += 2 * KeySize + Spacing;
  
  _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize * 3, KeySize ),
			      "SPACE", KeyButton::Space ) );
  x += 3 * KeySize + Spacing;
  
  _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize * 2, KeySize ),
			      "BS", KeyButton::Backspace ) );
  x += 2 * KeySize + Spacing;
  
  _keys.push_back( KeyButton( Tme::FloatRect( x, y, KeySize * 2, KeySize ),
			      "OK", KeyButton::Enter ) );
}

Keyboard::~Keyboard()
{
}

void Keyboard::setListener( KeyboardListener* listener )
{
  _listener = listener;
}

void Keyboard::setTitle( const std::string& title )
{
  _title = title;
}

void Keyboard::setString( const std::string& string )
{
  //_string = string;
  strncpy( _string, string.c_str(), sizeof( _string ) );
  _cursor = strlen( _string );
}

std::string Keyboard::getString() const
{
  return _string;
}

void Keyboard::update()
{
  _ticks++;
  _keyRepeater.update();

  const unsigned FadeTicks = 300 / Period;

  switch( _state ) {

  case FadeIn:
    _alpha = getFadeInAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {

      setTransitionState( TransitionInDone );
      setState( Idle );
    }
    break;

  case Idle:
    {
      for( unsigned i = 0; i < _keys.size(); ++i )
	_keys[ i ].update();
    }
    break;

  case FadeOut:
    _alpha = getFadeOutAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {
     
      //StateManager::instance()->popState();
      setTransitionState( TransitionOutDone );
    }
    break;
  }
}

void Keyboard::render()
{
  Menu::drawFade( 0, 0, 0, std::min( _alpha, (uint8_t) 180 ) );

  for( unsigned i = 0; i < _keys.size(); ++i )
    _keys[ i ].render( _alpha );

  int x;
  int y = Spacing;
  
  const int titleBoxH = 16;
  const int FontW = 10;
  const int FontH = 10;

  if( _title.size() ) {

    const int titleBoxW = ( _title.size() + 3  ) * FontW;
    x = ( ( (int) ScreenW ) - titleBoxW ) / 2;
    
    Menu::drawBox( x, y, titleBoxW / 8, titleBoxH / 8, Menu::ColorNormal, 255 );
    //g_font8->printBlended( x + 12, Spacing * 2, _alpha, _title.c_str() );
    g_font10->printBlended( x + ( 3 * FontW ) / 2, y + ( titleBoxH - FontH ) / 2, _alpha, _title.c_str() );
  }
  y += titleBoxH + Spacing;

  const int textBoxW = 24 * FontW;
  x = ( ( (int) ScreenW ) - textBoxW ) / 2;
  //Tme::Video::drawSolidRect( Tme::FloatRect( x, 10, 20 * 8, 24 ),
  //			     BackgroundR,
  //			     BackgroundG,
  //			     BackgroundB,
  //			     BackgroundA );
  Menu::drawBox( x, y, textBoxW / 8, KeySize / 8, Menu::ColorNormal, 255 );

  x = ( ScreenW - FontW * strlen( _string ) ) / 2;
  y += ( KeySize - FontH ) / 2;

  if( getBlink( BlinkMedium, _ticks ) ) {
    
    Tme::Video::drawSolidRect( Tme::FloatRect( x + _cursor * FontW, y, FontW, FontH ),
			       0xFF, 0xFF, 0x00, _alpha );
  }
  
  //g_font8->printBlended( x, y, _alpha, _string );
  g_font10->printBlended( x, y, _alpha, _string );
}

void Keyboard::handleClick( int x, int y )
{
  if( _state != Idle )
    return;

  for( unsigned i = 0; i < _keys.size(); ++i ) {

    if( !_keys[ i ].geometry.contains( x, y ) )
      continue;

    handleKeyButton( &( _keys[ i ] ) );
  }
}

void Keyboard::handleKey( int key )
{
  if( _state != Idle )
    return;

  _keyRepeater.handleKey( key );

  switch( key ) {

  case KeyLeft:
    _keys[ _currentButton ].setFocus( false );
    _currentButton = getNextIndex( _currentButton, Left );
    _keys[ _currentButton ].setFocus( true );
    break;

  case KeyRight:
    _keys[ _currentButton ].setFocus( false );
    _currentButton = getNextIndex( _currentButton, Right );
    _keys[ _currentButton ].setFocus( true );
    break;

  case KeyUp:
    _keys[ _currentButton ].setFocus( false );
    _currentButton = getNextIndex( _currentButton, Up );
    _keys[ _currentButton ].setFocus( true );
    break;

  case KeyDown:
    _keys[ _currentButton ].setFocus( false );
    _currentButton = getNextIndex( _currentButton, Down );
    _keys[ _currentButton ].setFocus( true );
    break;

  case KeyOk:
    handleKeyButton( &( _keys[ _currentButton ] ) );
    break;

  case KeyBack:
    handleKeyButton( &( _keys[ _keys.size() - 2 ] ) );
    break;
  }
}
  
// void Keyboard::enter()
// {
//   setState( FadeIn );

//   _currentButton = _keys.size() / 2;
//   for( int i = 0; i < (int) _keys.size(); ++i ) {

//     _keys[ i ].setState( KeyButton::Idle );
//     _keys[ i ].setFocus( i == _currentButton );
//   }
// }

void Keyboard::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    if( getTransitionType() == TransitionTop ) {
    
      _currentButton = _keys.size() / 2;
      for( int i = 0; i < (int) _keys.size(); ++i ) {
	
	_keys[ i ].setState( KeyButton::Idle );
	_keys[ i ].setFocus( i == _currentButton );
      }
    }

    setState( FadeIn );
  }
  else if( getTransitionState() == TransitionOut ) {

    setState( FadeOut );
  }
}

static float getCenterX( const Tme::FloatRect& r )
{
  return r.x() + r.w() * 0.5f;
}

int Keyboard::getNextIndex( const int index, const Dir dir )
{
  const int NumRows = 6;
  const int rowCounts[ NumRows ] = {
    
    10, 11, 10, 11, 8, 5
  };
  
  int row = 0;
  int rowFirstIndex = 0;
  int col = index;
  while( col >= rowCounts[ row ] ) {

    col -= rowCounts[ row ];
    rowFirstIndex += rowCounts[ row ];
    row++;
  }
  
  int nextRow = row;;

  switch( dir ) {
  case Left:
    return rowFirstIndex + ( col + rowCounts[ row ] - 1 ) % rowCounts[ row ];
  case Right:
    return rowFirstIndex + ( col + 1 ) % rowCounts[ row ];
  case Up:
    nextRow = ( row + NumRows - 1 ) % NumRows;
    break;
  case Down:
    nextRow = ( row + 1 ) % NumRows;
    break;
  }

  int nextRowFirstIndex = 0;
  for( int i = 0; i < nextRow; ++i )
    nextRowFirstIndex += rowCounts[ i ];

  int nextRowLastIndex = nextRowFirstIndex + rowCounts[ nextRow ] - 1;
  
  float centerX = getCenterX( _keys[ index ].geometry );
  float minDX = 9999;
  int minIndex = nextRowFirstIndex;
  for( int i = nextRowFirstIndex; i <= nextRowLastIndex; ++i ) {
    
    float dx = fabsf( centerX - getCenterX( _keys[ i ].geometry ) );
    if( dx < minDX || ( row < nextRow && dx <= minDX ) ) {

      minDX = dx;
      minIndex = i;
    }
  }

  return minIndex;
}

void Keyboard::handleKeyButton( KeyButton* button )
{
  int length = strlen( _string );

  switch( button->function ) {

  case KeyButton::Character:
    if( _cursor < ( MaxLength - 1 ) ) {
	
      _string[ _cursor++ ] = button->character;

      if( _cursor >= length )
	_string[ _cursor ] = 0;

      button->setState( KeyButton::Ok );
    }
    else {

      button->setState( KeyButton::Error );
    }
    break;

  case KeyButton::Space:
    if( _cursor < ( MaxLength - 1 ) ) {

      _string[ _cursor++ ] = ' ';

      if( _cursor >= length )
	_string[ _cursor ] = 0;

      button->setState( KeyButton::Ok );
    }
    else {

      button->setState( KeyButton::Error );
    }
    break;

  case KeyButton::Backspace:
    if( _cursor > 0 ) {

      _cursor--;
      for( int pos = _cursor; pos <= length; ++pos ) {

	_string[ pos ] = _string[ pos + 1 ];
      }

      button->setState( KeyButton::Ok );
    }
    else {

      button->setState( KeyButton::Error );
    }
    break;

  case KeyButton::Left:
    if( _cursor > 0 ) {

      _cursor--;
      button->setState( KeyButton::Ok );
    }
    else {

      button->setState( KeyButton::Error );
    }
    break;

  case KeyButton::Right:
    if( _cursor < length ) {

      _cursor++;
      button->setState( KeyButton::Ok );
    }
    else {

      button->setState( KeyButton::Error );
    }
    break;
    
  case KeyButton::Enter:

    //playSFX( SFX_Uncover );
    button->setState( KeyButton::Ok );

    if( length == 0 ) {
	
      snprintf( _string, sizeof( _string ), "%s", GameTitle );
    }
      
    //StateManager::instance()->popState();

    if( nextState ) {

      g_stateManager.swapTopState( nextState );
    }
    else {

      g_stateManager.popState();
    }

    if( _listener ) {
	
      _listener->onTextEntered( _string );
    }
    //setState( FadeOut );
    
    break;
  }
}

void Keyboard::setState( State state )
{
  _ticks = 0;
  _state = state;

  switch( state ) {

  case FadeIn:
    _alpha = 0;
    break;

  case Idle:
    break;

  case FadeOut:
    _alpha = 255;
  }
}
