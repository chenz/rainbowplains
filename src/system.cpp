#include "globals.h"
#include "system.h"

bool AbstractSystem::isFlagButton( unsigned button ) const
{
  return mapJoyButton(button) == KeyFlag;
}

bool AbstractSystem::isFlagKey( unsigned key ) const
{
  return mapKey(key) == KeyFlag;
}

void AbstractSystem::checkVolume()
{
  if( ( g_keys & ( 1 << KeyVolumePlus ) && !( g_keysPrev & ( 1 << KeyVolumePlus  ) ) ) ) {

    volumeUp();
  }
  if( ( g_keys & ( 1 << KeyVolumeMinus ) && !( g_keysPrev & ( 1 << KeyVolumeMinus  ) ) ) ) {

    volumeDown();
  }
}
