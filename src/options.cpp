#include "options.h"
#include "propertiesfile.h"

#include <algorithm>

#include <stdio.h>

Options::Options()
  : _bgmVolume(5)
  , _bgmEnabled(true)
  , _bgmShuffle(true)
  , _sfxVolume(5)
  , _sfxEnabled(true)
{
  _bgmPath.push_back("../bgm");
}

void Options::setBGMVolume( int value )
{
  _bgmVolume = std::min( std::max( value, 0 ), 10 );
}

int Options::getBGMVolume() const
{
  return _bgmVolume;
}

void Options::setBGMEnabled( bool value )
{
  _bgmEnabled = value;
}

bool Options::isBGMEnabled() const
{
  return _bgmEnabled;
}

void Options::setSFXVolume( int value )
{
  _sfxVolume = std::min( std::max( value, 0 ), 10 );
}

int Options::getSFXVolume() const
{
  return _sfxVolume;
}

void Options::setSFXEnabled( bool value )
{
  _sfxEnabled = value;
}

bool Options::isSFXEnabled() const
{
  return _sfxEnabled;
}

void Options::setBGMShuffle( bool value )
{
  _bgmShuffle = value;
}

bool Options::isBGMShuffle() const
{
  return _bgmShuffle;
}

const std::list<std::string>& Options::getBGMPath() const
{
  return _bgmPath;
}

void Options::load(PropertiesFile* propertiesFile)
{
  setBGMVolume(propertiesFile->getInt("bgm.volume", 5));
  _bgmEnabled = propertiesFile->getInt("bgm.enabled", 1) == 1;
  _bgmShuffle = propertiesFile->getInt("bgm.shuffle", 1) == 1;
  setSFXVolume(propertiesFile->getInt("sfx.volume", 5));
  _sfxEnabled = propertiesFile->getInt("sfx.enabled", 1) == 1;

  std::list<std::string> bgmPath;

  for(int i = 0; i < 10; ++i) {
    
    char buffer[1024];
    snprintf(buffer, sizeof(buffer), "bgm.path.entry%d", i);
    
    std::string entry = propertiesFile->getString(buffer);
    if( !entry.size() )
      break;
    
    bgmPath.push_back(entry);
  }

  if( bgmPath.size() )
    _bgmPath = bgmPath;
}

void Options::save(PropertiesFile* propertiesFile)
{
  propertiesFile->setInt( "bgm.volume", _bgmVolume );
  propertiesFile->setInt( "bgm.enabled", _bgmEnabled ? 1 : 0 );
  propertiesFile->setInt( "bgm.shuffle", _bgmShuffle ? 1 : 0 );
  propertiesFile->setInt( "sfx.volume", _sfxVolume );
  propertiesFile->setInt( "sfx.enabled", _sfxEnabled ? 1 : 0 );

  int i = 0;
  for( std::list<std::string>::const_iterator it = _bgmPath.begin();
       it != _bgmPath.end(); ++it ) {

    char buffer[1024];
    snprintf(buffer, sizeof(buffer), "bgm.path.entry%d", i++);
    
    propertiesFile->setString(buffer, *it);
  }
}
