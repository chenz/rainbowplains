#include "globals.h"
#include "menu.h"
#include "sprites.h"

#include <math.h>

unsigned Menu::roundSize(unsigned s)
{
  const unsigned d = s / 8;
  const unsigned r = s % 8;
  return std::max(static_cast<unsigned>(24), d * 8 + ( r ? 8 : 0 ));
}

void Menu::drawBox( const int x0, const int y0, const int cols, const int rows, 
		    BoxColor color, uint8_t alpha )
{
  int offset = 0;
  switch( color ) {

  case ColorRed:
    offset = SPRITE_REDBOX_TOPLEFT - SPRITE_BOX_TOPLEFT;
    break;
  case ColorGreen:
    offset = SPRITE_GREENBOX_TOPLEFT - SPRITE_BOX_TOPLEFT;
    break;
  default: 
    break;
  }
  
  Tme::Video::blitSprite(     SPRITE_BOX_TOPLEFT + offset,                    x0,                    y0, alpha );
  Tme::Video::blitSprite(    SPRITE_BOX_TOPRIGHT + offset, x0 + ( cols - 1 ) * 8,                    y0, alpha );
  Tme::Video::blitSprite(  SPRITE_BOX_BOTTOMLEFT + offset,                    x0, y0 + ( rows - 1 ) * 8, alpha );
  Tme::Video::blitSprite( SPRITE_BOX_BOTTOMRIGHT + offset, x0 + ( cols - 1 ) * 8, y0 + ( rows - 1 ) * 8, alpha );
  
  int x;
  int y;

  x = x0 + 8;
  for( int i = 0; i < cols - 2; ++i ) {

    Tme::Video::blitSprite(    SPRITE_BOX_TOP + offset, x,                    y0, alpha );
    Tme::Video::blitSprite( SPRITE_BOX_BOTTOM + offset, x, y0 + ( rows - 1 ) * 8, alpha );
    x += 8;
  }
  
  y = y0 + 8;
  for( int i = 0; i < rows - 2; ++i ) {

    Tme::Video::blitSprite(  SPRITE_BOX_LEFT + offset,                    x0, y, alpha );
    Tme::Video::blitSprite( SPRITE_BOX_RIGHT + offset, x0 + ( cols - 1 ) * 8, y, alpha );
    y += 8;
  }

  y = y0 + 8;
  for( int j = 0; j < rows - 2; ++j ) {
    
    x = x0 + 8;
    for( int i = 0; i < cols - 2; ++i ) {
      
      Tme::Video::blitSprite( SPRITE_BOX_CENTER + offset, x, y, alpha );
      x += 8;
    }

    y += 8;
  }
}

void Menu::drawFrame( const int x0, const int y0, const int cols, const int rows, uint8_t alpha )
{
  Tme::Video::blitSprite(     SPRITE_BOXFRAME_TOPLEFT,                x0 - 8,                y0 - 8, alpha );
  Tme::Video::blitSprite(    SPRITE_BOXFRAME_TOPRIGHT, x0 + ( cols - 1 ) * 8,                y0 - 8, alpha );
  Tme::Video::blitSprite(  SPRITE_BOXFRAME_BOTTOMLEFT,                x0 - 8, y0 + ( rows - 1 ) * 8, alpha );
  Tme::Video::blitSprite( SPRITE_BOXFRAME_BOTTOMRIGHT, x0 + ( cols - 1 ) * 8, y0 + ( rows - 1 ) * 8, alpha );

  int x = x0 + 8;
  for( int i = 0; i < cols - 2; ++i ) {

    Tme::Video::blitSprite(    SPRITE_BOXFRAME_TOP, x,        y0 - 8, alpha );
    Tme::Video::blitSprite( SPRITE_BOXFRAME_BOTTOM, x, y0 + rows * 8, alpha );
    x += 8;
  }

  int y = y0 + 8;
  for( int i = 0; i < rows - 2; ++i ) {

    Tme::Video::blitSprite(  SPRITE_BOXFRAME_LEFT,        x0 - 8, y, alpha );
    Tme::Video::blitSprite( SPRITE_BOXFRAME_RIGHT, x0 + cols * 8, y, alpha );
    y += 8;
  }
}

void Menu::drawFade( const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a )
{
  Tme::Video::drawSolidRect( Tme::FloatRect( 0, 0, ScreenW, ScreenH ), r, g, b, a );
}

Menu::Menu( /*const Tme::FloatRect& geometry*/ /*, const std::vector< std::string >& buttonTexts*/ STM::StateManager* manager )
  : ::State( manager )
  , listener( 0 )
  , allowBack( false )
  , buttonIndex( -1 )
  , clickedIndex( -1 )
{
  //const int Spacing = 6;
  //const int ButtonW = geometry.w();
  //const int ButtonH = ( geometry.h() - ( buttonTexts.size() - 1 ) * Spacing ) / buttonTexts.size();
  //
  //int x = geometry.x();
  //int y = geometry.y();
  //for( unsigned i = 0; i < buttonTexts.size(); ++i ) {
  //
  //  buttons.push_back( Button( Tme::FloatRect( x, y, ButtonW, ButtonH ), buttonTexts[ i ] ) );
  //  y += ButtonH + Spacing;
  //}
}

unsigned Menu::addButtons( const std::vector< std::string >& texts,
			   int x, int y, 
			   int w, int h,
			   int spacing )
{
  unsigned i0 = buttons.size();

  w = roundSize(w);
  h = roundSize(h);
  
  unsigned i = 0;
  for( ; i < texts.size(); ++i ) {

    buttons.push_back( Button( Tme::FloatRect( x, y, w, h ), texts[ i ] ) );
    y += h + spacing;
  }

  buttonIndex = std::max( 0, buttonIndex );

  return i0;
};

void Menu::update()
{
  //if( StateManager::instance()->topState() != this )
  //  return;
  if( !isTopState() )
    return;

  ticks++;

  const int FadeTicks = 500 / Period;

  switch( _state ) {

  case FadeIn:
    alpha = getFadeInAlpha( ticks, FadeTicks );
    if( ticks >= FadeTicks ) {
      
      setTransitionState( TransitionInDone );
      setState( Idle );
    }
    break;
    
  case Idle:
    alpha = 255;
    break;

  case FadeOut:
    alpha = getFadeOutAlpha( ticks, FadeTicks );
    if( ticks >= FadeTicks ) {
      
      //StateManager::instance()->popState();
      setTransitionState( TransitionOutDone );
      //if( listener )
      //  listener->onMenuFinished( clickedIndex );
    }
    break;
  }
  
  for( unsigned i = 0; i < buttons.size(); ++i ) {

    buttons[ i ].update();
  }
}

void Menu::render()
{
  for( unsigned i = 0; i < buttons.size(); ++i )
    buttons[ i ].render( alpha );
}

void Menu::handleClick( int x, int y )
{
  switch( _state ) {
    
  case FadeIn:
  case Idle:
    for( unsigned i = 0; i < buttons.size(); ++i ) {
      
      if( !buttons[ i ].geometry.contains( x, y ) )
	continue;
      
      //playSFX( SFX_Uncover );

      clickedIndex = (int) i;
      buttons[ i ].setState( Button::Ok );

      g_stateManager.popState();

      if( listener )
        listener->onMenuFinished( clickedIndex );

      break;
    }
    break;
    
  case FadeOut:
    break;
  }
}

void Menu::handleKey( int key )
{
  assert( buttonIndex >= 0 );
  
  switch( _state ) {
    
  case FadeIn:
  case Idle:
    
    switch( key ) {

    case KeyUp:
      {
	Button* next = buttons[ buttonIndex ].neighbours[ Button::Top ];
	if( !next ) 
	  return;
	
	playSFX( SFX_Uncover );
	buttons[ buttonIndex ].setFocus( false );
	buttonIndex = getButtonIndex( next );
	buttons[ buttonIndex ].setFocus( true );
      }
      break;

    case KeyDown:
      {
	Button* next = buttons[ buttonIndex ].neighbours[ Button::Bottom ];
	if( !next ) 
	  return;
	
	playSFX( SFX_Uncover );
	buttons[ buttonIndex ].setFocus( false );
	buttonIndex = getButtonIndex( next );
	buttons[ buttonIndex ].setFocus( true );
      }
      break;
      
    case KeyOk:
      //playSFX( SFX_Uncover );
      clickedIndex = buttonIndex;
      buttons[ buttonIndex ].setState( Button::Ok );
      //setState( FadeOut );

      g_stateManager.popState();

      if( listener )
        listener->onMenuFinished( clickedIndex );

      break;

    case KeyMenu:
      if( allowBack )
	g_stateManager.popState();
      break;
    }
  case FadeOut:
    break;
  }
}

// void Menu::enter()
// {
//   ticks = 0;
//   alpha = 1.0;
//   clickedIndex = buttonIndex;
  
//   for( unsigned i = 0; i < buttons.size(); ++i ) {

//     buttons[ i ].setFocus( ( (int) i ) == buttonIndex );
//     buttons[ i ].setState( Button::Idle );
//   }

//   setState( FadeIn );
// }

void Menu::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    if( getTransitionType() == TransitionTop ) {
      
      ticks = 0;
      alpha = 1.0;
      clickedIndex = buttonIndex;
      
      for( unsigned i = 0; i < buttons.size(); ++i ) {
	
	buttons[ i ].setFocus( ( (int) i ) == buttonIndex );
	buttons[ i ].setState( Button::Idle );
      }
    }

    setState( FadeIn );
  }
  else if( getTransitionState() == TransitionOut ) {

    setState( FadeOut );
  }
}

int Menu::getButtonIndex( const Button* b ) const
{
  for( unsigned i = 0; i < buttons.size(); ++i ) {

    if( &( buttons[ i ] ) == b )
      return (int) i;
  }

  return -1;
}

void Menu::setState( State state )
{
  ticks = 0;
  _state = state;
}
