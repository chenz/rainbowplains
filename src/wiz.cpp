#if defined(SYSTEM_WIZ)

#include "globals.h"
#include "wiz.h"

#include <tmepp/config.h>
#include <tmepp/rendererwiz.h>

#include <SDL.h>

#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>

static unsigned batteryTicks = 0;
static bool batteryLow = false;

static bool checkBatteryLow()
{
    int fd = open( "/dev/pollux_batt", O_RDONLY );
    if( fd < 0 )
      return true;

    unsigned short value;
    read( fd, &value, sizeof( value ) );
    close( fd );

    return value == 0 || value == 4;
}

bool wiz_isBatteryLow()
{
  return batteryLow;
}

bool wiz_isQuit()
{
  return 
    ( g_buttons & ( 1 << WizMenuButton ) ) && 
    ( g_buttons & ( 1 << WizSelectButton ) );
}

void wiz_checkBattery()
{
  unsigned t = SDL_GetTicks();
  if( t - batteryTicks < 60 * 1000 ) {

    batteryTicks = t;
    batteryLow = checkBatteryLow();
  }
}

// Wiz implementation of system interface...

int SystemWiz::mapJoyButton( unsigned button ) const
{
  switch( button ) {
  case WizUpButton:
    return KeyUp;
  case WizLeftButton:
    return KeyLeft;
  case WizDownButton:
    return KeyDown;
  case WizRightButton:
    return KeyRight;
  case WizMenuButton:
    return KeyMenu;
  case WizAButton:
  case WizBButton:
    return KeyOk;
  case WizXButton:
  case WizYButton:
    return KeyBack;
  case WizLeftShoulderButton:
  case WizRightShoulderButton:
    return KeyFlag;
  case WizVolPlusButton:
    return KeyVolumePlus;
  case WizVolMinusButton:
    return KeyVolumeMinus;
  }

  return -1;
}

int SystemWiz::mapKey( unsigned /*key*/ ) const
{
  return -1;
}

static bool screensaverEnabled = false;

void SystemWiz::enableScreensaver( bool on ) 
{
  if( screensaverEnabled == on )
    return;

  const unsigned Off = 0;
  const unsigned On = 1;
  
  unsigned buffer[ 2 ] = { on ? Off : On, 0 };
  
  int fd = open( "/dev/fb0", O_RDWR );
  if( fd >= 0 ) {

    screensaverEnabled = on;
    
    ioctl( fd, _IOW( 'D', 90, unsigned[ 2 ] ), &buffer );
    close( fd );
  }
}

void SystemWiz::dbgShowStats()
{
  #if DEBUG
  #if TME_DEBUG
  Tme::RendererWiz::dbg_Stats last;
  Tme::RendererWiz::dbg_Stats average;
  Tme::RendererWiz::dbg_getStats( &last, &average );

  char buffer[ 100 ];
  snprintf( buffer, sizeof( buffer ), "%03u %05u %05u %u", 
	    last.time ? ( 1000 / last.time ) : 0,
	    last.numTexturedTris,
	    last.numColoredTris,
	    last.numStateChanges );
  g_font8->print( 0, ScreenH - 16, buffer );

  snprintf( buffer, sizeof( buffer ), "%03u %05u %05u %u", 
	    average.time ? ( 1000 / average.time ) : 0,
	    average.numTexturedTris,
	    average.numColoredTris,
	    average.numStateChanges );
  g_font8->print( 0, ScreenH - 8, buffer );
  #endif // TME_DEBUG
  #endif // DEBUG
}

#endif // defined(SYSTEM_WIZ)

