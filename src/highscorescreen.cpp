#include "globals.h"
#include "highscorescreen.h"
#include "menu.h"

#include <tmepp/tmepp.h>

HighscoreScreen::HighscoreScreen( STM::StateManager* manager )
  : ::State( manager )
  , _gameKey( Game::getDefaultGameKey( Game::Small ) )
  , _currentButton( 0 )
{
  const int Spacing = 24;
  int x = ( ScreenW - 2 * Spacing - 2 * Spacing - 2 * Spacing - 4 * Spacing ) / 2;
  int y = 210;

  _buttons[ ButtonPrev ] = Button( Tme::FloatRect( x, y, 2 * Spacing, 24 ), "<<" );

  x += 3 * Spacing;
  _buttons[ ButtonClose ] = Button( Tme::FloatRect( x, y, 4 * Spacing, 24 ), "CLOSE" );
  
  x += 5 * Spacing;
  _buttons[ ButtonNext ] = Button( Tme::FloatRect( x, y, 2 * Spacing, 24 ), ">>" );
}

void HighscoreScreen::setGameKey( const GameKey& gameKey )
{
  _gameKey = gameKey;
}

const int DelayTicks = 500 / Period;
const int FadeTicks = 1000 / Period;

void HighscoreScreen::update()
{
  _ticks++;

  switch( _state ) {

  case Delay:
    if( _ticks >= DelayTicks ) {

      setTransitionState( TransitionInDone );
      setState( Display );
    }
    break;

  case Display:
    break;

  case FadeOut:
    if( _ticks >= DelayTicks ) {
      
      //StateManager::instance()->popState();	
      //g_stateManager.popState();
      setTransitionState( TransitionOutDone );
    }
    break;
  }
  
  for( int i = 0; i < 3; ++i )
    _buttons[ i ].update();
}

void HighscoreScreen::render()
{
  //Tme::Video::drawSolidRect( Tme::FloatRect( 20, 10, 280, 190 ), 
  //			     BackgroundR, 
  //			     BackgroundG,
  //			     BackgroundB,
  //			     BackgroundA );

  const int BoxW = 280;
  const int BoxH = 196;
  
  const int BoxX = ( (int) ScreenW - BoxW ) / 2;

  uint8_t a = 255;
  if( _state == Delay ) {

    a = getFadeInAlpha( _ticks, DelayTicks );
  }
  else if( _state == FadeOut ) {

    a = getFadeOutAlpha( _ticks, DelayTicks );
  }

  Menu::drawBox( BoxX, 10, BoxW / 8, BoxH / 8, Menu::ColorNormal, a );
    
  //const int FontW = 8;
  const int FontW = 10;

  int x = ( ScreenW - 24 * FontW ) / 2;
  int y = 20;
  
  //g_font10->printBlended( x, y, a, "HIGHSCORES......" );
  
  g_font10->printBlended( x, y, a, "SIZE %2d X %2d - %3d BOMBS", _gameKey.width, _gameKey.height, _gameKey.numBombs );

  for( int i = 0; i < 3; ++i )
    _buttons[ i ].render( a );

  if( _state != Delay ) {
    
    const int LineW = ( 2 + 1 + 16 + 2 + 5 ) * FontW;

    x = ( ScreenW - LineW ) / 2;
    y = ( ScreenH - 10 * 16 ) / 2;
    
    const std::vector<Highscores::Entry>& scores = g_highscores->entries[_gameKey];

    for( int i = 0; i < 10; ++i ) {
     
      // Blink current entry...
      if( ( _gameKey != g_highscores->lastGameKey ) ||
	  ( i != g_highscores->lastPosition ) ||
	  ( _ticks < FadeTicks ) ||
	  getBlink( BlinkMedium, _ticks - FadeTicks ) ) {
	
	if( _state == Display ) {
	  
	  a = 0;
	  int threshold = ( _state == Display ) ? ( ( 9 - i ) * FadeTicks / 10 ) : ( i * FadeTicks / 10 );
	  if( _ticks > threshold ) {
	    
	    a = getFadeInAlpha( _ticks - threshold, FadeTicks / 5 );
	  }
	}
	
	char buffer[ 32 ];
	
	snprintf( buffer, sizeof( buffer ), "%.02d", 1 + i );
	//g_font8->printBlended( x         , y, a, buffer );
	//g_font8->printBlended( x +  4 * 8, y, a, g_highscores->entries[ _size ][ i ].name.c_str() );
	g_font10->printBlended( x            , y, a, buffer );
	g_font10->printBlended( x + 3 * FontW, y, a, scores[ i ].name.c_str() );
	
	int seconds = ( scores[ i ].time / 1000 ) % 60;
	int minutes =  scores[ i ].time / 60000;
	snprintf( buffer, sizeof( buffer ), "%.02d:%02d", minutes, seconds );
	//g_font8->printBlended( x + 24 * 8, y, a, buffer );
	g_font10->printBlended( x + 21 * FontW, y, a, buffer );
      }
    
      y += 16;
    }
  }
}

void HighscoreScreen::handleClick( int x, int y )
{
  for( int i = 0; i < 3; ++i ) {

    if( !_buttons[ i ].geometry.contains( x, y ) )
      continue;

    //playSFX( SFX_Uncover );
    
    _buttons[ i ].setState( Button::Ok );

    switch( i ) {

    case ButtonPrev:
      _gameKey = g_highscores->getPrevKey(_gameKey);
      setState( Display );
      break;

    case ButtonNext:
      _gameKey = g_highscores->getNextKey(_gameKey);
      setState( Display );
      break;
      
    case ButtonClose:
      //setState( FadeOut );
      if( nextState ) {

	g_stateManager.swapTopState( nextState );
      }
      else {

	g_stateManager.popState();
      }
      break;
    }
  }
}

void HighscoreScreen::handleKey( int key )
{
  switch( key ) {
    
  case KeyLeft:
    playSFX( SFX_Uncover );
    _buttons[ _currentButton ].setFocus( false );
    _currentButton = ( _currentButton + 2 ) % 3;
    _buttons[ _currentButton ].setFocus( true );
    break;

  case KeyRight:
    playSFX( SFX_Uncover );
    _buttons[ _currentButton ].setFocus( false );
    _currentButton = ( _currentButton + 1 ) % 3;
    _buttons[ _currentButton ].setFocus( true );
    break;
      
  case KeyOk:
    //playSFX( SFX_Uncover );
    _buttons[ _currentButton ].setState( Button::Ok );

    switch( _currentButton ) {

    case ButtonPrev:
      _gameKey = g_highscores->getPrevKey(_gameKey);
      setState( Display );
      break;
      
    case ButtonNext:
      _gameKey = g_highscores->getNextKey(_gameKey);
      setState( Display );
      break;
      
    case ButtonClose:
      //setState( FadeOut );
      if( nextState ) {

	g_stateManager.swapTopState( nextState );
      }
      else {

	g_stateManager.popState();
      }
      break;
    }
    break;

  case KeyMenu:
    g_stateManager.popState();
    break;
  }
}

// void HighscoreScreen::enter()
// {
//   _currentButton = 1;
//   for( int i = 0; i < 3; ++i ) {

//     _buttons[ i ].setFocus( i == _currentButton );
//     _buttons[ i ].setState( Button::Idle );
//   }
  
//   setState( Delay );
// }

void HighscoreScreen::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    _currentButton = 1;
    for( int i = 0; i < 3; ++i ) {
      
      _buttons[ i ].setFocus( i == _currentButton );
      _buttons[ i ].setState( Button::Idle );
    }

    setState( Delay );
  }
  else if( getTransitionState() == TransitionOut ) {

    setState( FadeOut );
  }
}

void HighscoreScreen::setState( State state )
{
  _ticks = 0;
  _state = state;

  switch( state ) {

  case Delay:
    break;

  case Display:
    break;

  case FadeOut:
    break;
  }
}
