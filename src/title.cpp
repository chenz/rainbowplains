#include "customgamemenu.h"
#include "game.h"
#include "globals.h"
#include "hue.h"
#include "keyboard.h"
#include "menu.h"
#include "soundmenu.h"
#include "sprites.h"
#include "title.h"

#include <tmepp/tmepp.h>

const int SpriteIdsRainbow[] = { 
  SPRITE_LOGO_R,
  SPRITE_LOGO_A,
  SPRITE_LOGO_I,
  SPRITE_LOGO_N,
  SPRITE_LOGO_B,
  SPRITE_LOGO_O,
  SPRITE_LOGO_W,
  0
};

const int SpriteIdsPlains[] = { 
  SPRITE_LOGO_P,
  SPRITE_LOGO_L,
  SPRITE_LOGO_A,
  SPRITE_LOGO_I,
  SPRITE_LOGO_N,
  SPRITE_LOGO_S,
  0
};


const int MenuW = Menu::roundSize(120);
const int SideW = (ScreenW - MenuW) / 2;
const int MaxLogoCharW = 40;

void drawSineLogo( int x, int y, Uint8 alpha, float arg, const int* spriteIds )
{
  const float dArg = 0.8f;
  const int RowH = 30;

  for( ; *spriteIds; spriteIds++ ) {
    
    const int dx = sinf(arg) * 16.0;
    const int dy = cosf(arg) * 8.0;
    arg += dArg;

    Tme::Sprite* s = Tme::Video::spriteTable.get(*spriteIds);
    if( !s )
      break;

    const int offset = (MaxLogoCharW - s->width()) / 2;

    s->blit( x + offset + dx, y + dy, alpha );
    y += RowH;
  }
}


Title::Title( STM::StateManager* manager )
  : ::State( manager )
  , _sineTicks(0)
{
  assert( !g_title );
  g_title = this;

  _customGameMenu = new CustomGameMenu( manager );
  _customGameMenu->listener = this;

  _highscoreScreen = new HighscoreScreen( manager );

  _soundMenu = new SoundMenu( manager );
  _soundMenu->listener = this;

  //mapW = /*8 +*/ ( ScreenW / 8 );
  //mapH = /*8 +*/ ( ScreenH / 8 );
  //tiles = new uint8_t[ mapW * mapH ];

  std::vector< std::string > menuButtons;
  menuButtons.push_back( "SMALL GAME" );
  menuButtons.push_back( "MEDIUM GAME" );
  menuButtons.push_back( "LARGE GAME" );
  menuButtons.push_back( "CUSTOM" );
  menuButtons.push_back( "HIGHSCORES" );
  menuButtons.push_back( "OPTIONS" );
  menuButtons.push_back( "QUIT" );
  
  //const int MenuH = ( menuButtons.size() * 24 ) + ( menuButtons.size() - 1 ) * 6; // Spacing

  menu = new Menu( manager );
  menu->addButtons( menuButtons,
		    ( ScreenW - MenuW ) / 2, 16, /*(ScreenH - MenuH ) / 2,*/
		    MenuW, 24, 
		    6 );
  
  for( unsigned i = 0; i < menuButtons.size(); ++i ) {

    menu->buttons[ i ].setNeighbour( Button::Bottom, 
				     &( menu->buttons[ ( i + 1 ) % menuButtons.size() ] ) );
  }

  menu->listener = this;

  //_backgroundHues[ 1 ].setHue( 90 );
}

Title::~Title()
{
  //delete [] tiles;
  delete menu;
}

void Title::update()
{
  //static int shiftCount( 0 );

  _ticks++;
  
  //_backgroundHues[ 0 ].shiftHue( 1 );
  //_backgroundHues[ 1 ].shiftHue( 1 );
  _backgroundHueTicks++;
  _sineTicks++;

  //shiftCount++;
  //if( shiftCount >= 100 / Period ) {
  //  
  //  shiftCount = 0;
  //  shiftColors();
  //}

  const int FadeTicks = 1000 / Period;

  //scrollX -= 60.0f / (const float) Period;
  //while( scrollX < -64.0f ) scrollX += 64.0f;

  switch( state ) {

  case FadeIn:
    alpha = getFadeInAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {
   
      setTransitionState( TransitionInDone );
      setState( FadeInLogo );
    }
    break;
    
  case FadeInLogo:
    alpha = getFadeInAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {
      
      setState( Idle );
    }
    break;
    
  case Idle:
    alpha = 255;
    break;
    
  case FadeOut:
    alpha = getFadeOutAlpha( _ticks, FadeTicks );
    if( _ticks >= FadeTicks ) {
      
      //StateManager::instance()->popState();
      setTransitionState( TransitionOutDone );
    }
  }
}

void Title::render()
{
//   float y = 0;
//   unsigned k = 0;
//   for( unsigned j = 0; j < mapH; ++j ) {
//     float x = 0; /*scrollX;*/
//     for( unsigned i = 0; i < mapW; ++i ) {
//
//       Tme::Video::blitSprite( tiles[ k++ ], x, y );
//       x += 8;
//     }
//     y += 8;
//   }

  renderBackground();

  if( state != FadeIn && (isTopState() || menu->isTopState()) ) {

    //Tme::Video::blitSprite( SPRITE_LOGO, ( ScreenW - 184 ) / 2, 10, ( state == FadeInLogo ) ? alpha : 255 );
    //Tme::Video::blitSprite( SPRITE_RAINBOW_PLAINS, ( ScreenW - 200 ) / 2, 10, ( state == FadeInLogo ) ? alpha : 255 );

    const int Offset0 = (SideW - MaxLogoCharW) / 2;
    const int Offset1 = SideW + MenuW + (SideW - MaxLogoCharW) / 2;

    drawSineLogo( Offset0, (ScreenH - 7 * 30) / 2,
		  ( state == FadeInLogo ) ? alpha : 255,
		  _sineTicks * 0.08f,
		  SpriteIdsRainbow );

    drawSineLogo( Offset1, (ScreenH - 6 * 30) / 2,
		  ( state == FadeInLogo ) ? alpha : 255,
		  (_sineTicks + 100) * 0.08f,
		  SpriteIdsPlains );

    char buffer[100];
    snprintf(buffer, sizeof(buffer), "V%s", VERSION);
    
    g_font10->print( ScreenW - 2 - strlen(buffer) * 10, ScreenH - 10, buffer );
  }

  
  
  if( state == FadeIn ||
      state == FadeOut ) {
    
    Menu::drawFade( 0, 0, 0, 255 - alpha );
  }
}

// void Title::enter()
// {
//   //scrollX = 0;
//   //createRandomCheckboard();
//   setState( FadeIn );
// }

void Title::onTransition( STM::State* prevState )
{
  if( getTransitionState() == TransitionIn ) {

    if( getTransitionType() != TransitionChild ) {
      
      //scrollX = 0;
      //createRandomCheckboard();
      setState( FadeIn );
    }
    else {

      setTransitionState( TransitionInDone );

      // Only go idle if there is no further transition queued...
      if( g_stateManager.isQueueEmpty() )
	setState( Idle );
    }
  }
  else if( getTransitionState() == TransitionOut ) {

    if( getTransitionType() != TransitionChild ) {

      setState( FadeOut );
    }
    else {

      setTransitionState( TransitionOutDone );
    }
  }
}

void Title::setState( State state )
{
  _ticks = 0;

  switch( state ) {

  case FadeIn:
    break;

  case FadeInLogo:
    alpha = 0;
    break;

  case Idle:
    //StateManager::instance()->pushState( menu );
    g_stateManager.pushState( menu );
    // Keyboard test...
    //StateManager::instance()->pushState( new Keyboard() );
    break;

  case FadeOut:
    break;
  }

  this->state = state;
}

void Title::onMenuFinished( int index )
{
  if( g_stateManager.getTopState() == menu ) {

    switch( index ) {
      
    case 0:
      g_game->setGameKey( Game::getDefaultGameKey(Game::Small) );
      //StateManager::instance()->setNextState( g_game );
      g_stateManager.swapTopState( g_game );
      break;
    case 1:
      g_game->setGameKey( Game::getDefaultGameKey(Game::Medium) );
      //StateManager::instance()->setNextState( g_game );
      g_stateManager.swapTopState( g_game );
      break;
    case 2:
      g_game->setGameKey( Game::getDefaultGameKey(Game::Large) );
      //StateManager::instance()->setNextState( g_game );
      g_stateManager.swapTopState( g_game );
      break;
    case 3:
      g_stateManager.pushState(_customGameMenu);
      break;
    case 4:
      _highscoreScreen->nextState = menu;
      //StateManager::instance()->pushState( _highscoreScreen );
      //StateManager::instance()->setNextState( menu );
      g_stateManager.pushState( _highscoreScreen );
      return;
    case 5:
      g_stateManager.pushState(_soundMenu);
      break;
    default:
      g_stateManager.popState();
    }
  }
  else if( g_stateManager.getTopState() == _customGameMenu ) {

    if( index == CustomGameMenu::B_START ) {
      
      g_game->setGameKey( GameKey( getMapWidth( _customGameMenu->getSize() ), getMapHeight( _customGameMenu->getSize() ), _customGameMenu->getBombs() ) );
      g_stateManager.swapTopState( g_game );
    }
  }
  
  //setState( FadeOut );
}

// void Title::createRandomCheckboard()
// {
//   uint8_t c0 = rand() % 8;
//   uint8_t c1 = ( c0 + 4 ) % 8; //( c0 + 2 + rand() % 5 ) % 8;
//  
//   unsigned k = 0;
//   for( unsigned j = 0; j < mapH; ++j ) {
//     for( unsigned i = 0; i < mapW; ++i ) {
//
//       tiles[ k++ ] = SPRITE_TILE08_COVERED_0 + 
// 	( ( ( i + j ) & 1 ) ? c0 : c1 );
//     }
//   }
// }

// void Title::shiftColors()
// {
//   unsigned k = 0;
//   for( unsigned j = 0; j < mapH; ++j ) {
//     for( unsigned i = 0; i < mapW; ++i ) {
//      
//       uint8_t t = tiles[ k ];
//       t -= SPRITE_TILE08_COVERED_0;
//       t = ( t + 1 ) % 8;
//       tiles[ k++ ] = t + SPRITE_TILE08_COVERED_0;
//     }
//   }
// }

void Title::renderBackground()
{
  unsigned ticks = _backgroundHueTicks;

  int y = 0;
  for( unsigned j = 0; j < (unsigned) ScreenH / 8; ++j ) {
    
    Hue h0( ticks );
    Hue h1( ticks + 180 );
    ticks++;

    int x = 0;
    for( unsigned i = 0; i < (unsigned) ScreenW / 8; ++i ) {
     
      Hue* hue = ( ( i + j ) & 1 ) ? &h0 : &h1;
      Tme::Video::drawSolidRect( Tme::FloatRect( x, y, 8, 8 ), hue->red, hue->green, hue->blue, 255 );
      x += 8;
    }
    y += 8;
  }
}


