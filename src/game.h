#ifndef __GAME_H__
#define __GAME_H__

#include "gamekey.h"
//#include "highscorescreen.h"
#include "keyboard.h"
#include "keyrepeater.h"
#include "menu.h"
#include "state.h"

#include <stdint.h>

class HighscoreScreen;

class Game : public State, public MenuListener, public KeyboardListener
{
 public:

  enum Size {

    Small,
    Medium,
    Large,
  };

  static GameKey getDefaultGameKey(int size);

  Game( STM::StateManager* manager );
  ~Game();

  void setGameKey( const GameKey& key );

  // State API...
  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  //void enter();

  void onTransition( STM::State* prevState );

  // MenuListener API...
  void onMenuFinished( int index );

  // KeyboardListerner API...
  void onTextEntered( const std::string& text );

 private:

  enum State {

    FadeIn, // Opening state
    Start, // Running game
    Won, // Won game, delay and maybe do highscore entry
    Lost, // Lost game, delay 
    Highscores, // Highscore entry/display active
    Waiting, // Waiting for input to start new game
    Wipe,
    FadeOut, // Closing state
  };

  struct Tile
  {
    uint8_t color;
    bool bomb;
    bool covered;
    uint8_t count;
    bool flag;

    int uncoverCount;
  };

  void handleTileClick( Tile* t, bool flag );
  void updateCover();
  void createMap();
  void incBombCount( int x, int y );
  bool checkUncover( int i, int j ) const;
  bool isWon() const;
  void uncoverAll();
  void uncoverOne();
  void setState( State state );

  void renderWipe( const int ox, const int oy );

  State _state;
  State _prevState;

  unsigned _tileW;
  unsigned _tileH;
  unsigned _numBombs;
  unsigned _numFlagged;

  
  GameKey _gameKey;
  Tile* _map;
  unsigned _mapW;
  unsigned _mapH;
  unsigned _numTiles;
  unsigned _spriteBaseCovered;
  unsigned _spriteBaseUncovered;

  unsigned _ticks;
  unsigned _clock;

  unsigned _numCovered;

  uint8_t _alpha;
  uint8_t _lostWonAlpha;

  Menu* _menu;

  Keyboard* _keyboard;
  HighscoreScreen* _highscoreScreen;

  unsigned _cursorX;
  unsigned _cursorY;

  int _wipePos;

  KeyRepeater _keyRepeater;

  unsigned _flagAnimTicks;
  unsigned _flagAnimOffset;

  Tile* _explodedTile;
};

#endif // __GAME_H__
