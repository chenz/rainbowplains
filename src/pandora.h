#ifndef _PANDORA_H_
#define _PANDORA_H_

#if defined(SYSTEM_PANDORA)

#include "system.h"

class SystemPandora : public AbstractSystem
{
public:  
  int mapJoyButton( unsigned button ) const;
  int mapKey( unsigned key ) const;
  void enableScreensaver( bool on );
  void dbgShowStats();
};

#endif // defined(SYSTEM_Pandora)

#endif /* _PANDORA_H_ */
