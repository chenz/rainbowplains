#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <list>
#include <string>

class PropertiesFile;

class Options
{
public:
  Options();

  void setBGMVolume( int value );
  int getBGMVolume() const;

  void setBGMEnabled( bool value );
  bool isBGMEnabled() const;

  void setSFXVolume( int value );
  int getSFXVolume() const;

  void setSFXEnabled( bool value );
  bool isSFXEnabled() const;

  void setBGMShuffle( bool value );
  bool isBGMShuffle() const;

  const std::list<std::string>& getBGMPath() const;

  void load(PropertiesFile* propertiesFile);
  void save(PropertiesFile* propertiesFile);

private:
  int _bgmVolume;
  bool _bgmEnabled;
  bool _bgmShuffle;
  std::list< std::string > _bgmPath;
  int _sfxVolume;
  bool _sfxEnabled;
};

#endif /* _OPTIONS_H_ */
