#ifndef __HIGHSCORES_H__
#define __HIGHSCORES_H__

#include "game.h"
#include "gamekey.h"

#include <string>
#include <vector>

struct Highscores
{
  struct Entry
  {
    std::string name;
    unsigned time;
  };
  
  Highscores();

  int getPosition( const GameKey& key, unsigned time ) const;
  void setHighscore( const GameKey& key, const std::string& name, unsigned time );
  
  void reset(const GameKey& key);
  void reset();

  bool load();
  bool save();

  std::map< GameKey, std::vector<Entry> > entries;

  GameKey getNextKey(const GameKey& key) const;
  GameKey getPrevKey(const GameKey& key) const;
  
  GameKey lastGameKey;
  int lastPosition;
  std::string lastName;
};

#endif // __HIGHSCORES_H__
