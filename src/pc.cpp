// PC implementation of system interface...

#if defined(SYSTEM_PC)

#include "globals.h"
#include "pc.h"

#include <SDL.h>

int SystemPC::mapJoyButton( unsigned /*button*/ ) const
{
  return -1;
}

static const int keyMap[ MaxKeys ] = {
  
  SDLK_UP, // KeyUp
  SDLK_DOWN, // KeyDown
  SDLK_LEFT, // KeyLeft
  SDLK_RIGHT, // KeyRight
  SDLK_a, // KeyAction
  SDLK_s, // KeyBack
  SDLK_d, // KeyFlag
  SDLK_ESCAPE, // KeyMenu
  SDLK_PLUS, // KeyVolumePlus
  SDLK_MINUS, // KeyVolumeMinus
#if DEBUG
  SDLK_c, // KeyCheat
#else
  -1, // KeyCheat
#endif
};

int SystemPC::mapKey( unsigned key ) const
{
  for( int i = 0; i < MaxKeys; ++i ) {
    
    if( keyMap[ i ] == (int) key )
      return i;
  }
  
  return -1;
}

void SystemPC::enableScreensaver( bool /*on*/ )
{
}

void SystemPC::dbgShowStats()
{
}

#endif // defined(SYSTEM_PC)
