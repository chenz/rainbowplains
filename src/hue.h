#ifndef __HUE_H__
#define __HUE_H__

#include <stdint.h>

struct Hue
{
  Hue( unsigned degree = 0 );
  void setHue( unsigned degree );
  void shiftHue( unsigned degree ); 

  unsigned degree;
  uint8_t red;
  uint8_t green;
  uint8_t blue;
};

#endif // __HUE_H__
