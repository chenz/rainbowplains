#include "keyrepeater.h"
#include "state.h"

#include <string.h>

KeyRepeater::KeyRepeater( State* state, const unsigned initialDelay, const unsigned repeatDelay )
  : _state( state ), _initialDelay( initialDelay ), _repeatDelay( repeatDelay )
{
  memset( _ticks, 0, sizeof( _ticks ) );
}

void KeyRepeater::handleKey( unsigned key )
{
  _ticks[ key ] = 0;
}

void KeyRepeater::update()
{
  for( unsigned i = 0; i < MaxKeys; ++i ) {
    
    if( !( g_keys & ( 1 << i ) ) ) {

      _ticks[ i ] = 0;
      continue;
    }
    
    _ticks[ i ]++;
    
    if( _ticks[ i ] < _initialDelay )
      continue;

    unsigned ticks = _ticks[ i ] - _initialDelay;
    while( ticks >= _repeatDelay ) {

      ticks -= _repeatDelay;
      _state->handleKey( i );
    }
    
    _ticks[ i ] = _initialDelay + ticks;
  }
}
