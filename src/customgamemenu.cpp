#include "customgamemenu.h"
#include "game.h"
#include "globals.h"
#include "menu.h"
#include "options.h"
#include "propertiesfile.h"

const int FontW = 10;
const int FontH = 10;
const int Padding = 8;
const int Spacing = 8;

const int ButtonH = 24;
const int RowH = ButtonH + Spacing;

const int UpperButtonW = Menu::roundSize(2 * Padding + 2 * FontW);

// "MAP SIZE: MEDIUM": 16 chars
// "BOMBS:    999"

const int UpperMaxChars = 16;

const int UpperRowW = UpperMaxChars * FontW + 2 * Spacing + 2 * UpperButtonW;

const int UpperX0 = (ScreenW - UpperRowW) / 2;
const int UpperX1 = UpperX0 + UpperButtonW + Spacing;
const int UpperX2 = UpperX0 + UpperButtonW + Spacing + UpperMaxChars * FontW + Spacing;

CustomGameMenu::CustomGameMenu( STM::StateManager* manager )
  : ::State(manager)
  , listener(0)
  , _keyRepeater( this, 300 / Period, 100 / Period )
  , _currentButton(B_START)
{
  _size = g_propertiesFile->getInt( "custom_map.size", Game::Medium );
  _bombs = g_propertiesFile->getInt( "custom_map.bombs", getNumBombsDefault(Game::Medium) );

  int y = 60;

  {
    _buttons[ B_PREV_SIZE ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
    _buttons[ B_NEXT_SIZE ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
    y += RowH;
    
    _buttons[ B_LESS_BOMBS ] = Button( Tme::FloatRect( UpperX0, y, UpperButtonW, ButtonH ), "<<" );
    _buttons[ B_MORE_BOMBS ] = Button( Tme::FloatRect( UpperX2, y, UpperButtonW, ButtonH ), ">>" );
    y += RowH;
  }

  {
    // "START": 5 chars
    
    const int ButtonW = Menu::roundSize(8 * FontW + 2 * Padding);
    const int x = (ScreenW - ButtonW) / 2;

    _buttons[ B_START ] = Button( Tme::FloatRect( x, y, ButtonW, ButtonH ), "START" );
    y += RowH;

    _buttons[ B_BACK ] = Button( Tme::FloatRect( x, y, ButtonW, ButtonH ), "BACK" );
    y += RowH;
  }
}

const int FadeTicks = 500 / Period;

void CustomGameMenu::update()
{
  _keyRepeater.update();

  for( int i = 0; i < 6; ++i )
    _buttons[ i ].update();

  _ticks++;

  switch( _state ) {

  case FadeIn:
    if( _ticks >= FadeTicks ) {
 
      setTransitionState(TransitionInDone);
      setState(Active);
    }
    break;

  case Active:
    break;

  case FadeOut:
    if( _ticks >= FadeTicks ) {
      
      //setState(Active);
      setTransitionState(TransitionOutDone);
    }
    break;
  }
}

void CustomGameMenu::render()
{
  const int BoxW = Menu::roundSize(UpperRowW + 4 * Spacing);
  const int BoxH = Menu::roundSize(5 * RowH + 4 * Spacing);
  
  const int BoxX = ( (int) ScreenW - BoxW ) / 2;
  const int BoxY = ( 60 - RowH - (3 * Spacing) / 2 );
  
  uint8_t a = 255;
  if( _state == FadeIn ) {

    a = getFadeInAlpha( _ticks, FadeTicks );
  }
  else if( _state == FadeOut ) {

    a = getFadeOutAlpha( _ticks, FadeTicks );
  }

  Menu::drawBox( BoxX, BoxY, BoxW / 8, BoxH / 8, Menu::ColorNormal, a );
    
  //const int FontW = 8;
  //const int FontW = 10;

  int x = ( ScreenW - 11 * FontW ) / 2;
  int y = 60 - RowH + (ButtonH - FontH) / 2;
  
  g_font10->printBlended( x, y, a, "CUSTOM GAME" );
  y += RowH;

  for( int i = 0; i < 6; ++i )
    _buttons[ i ].render( a );
  
  const char* sizeText(0);
  switch( _size ) {

  case Game::Small:
    sizeText = "SMALL";
    break;
  case Game::Medium:
    sizeText = "MEDIUM";
    break;
  case Game::Large:
    sizeText = "LARGE";
    break;
  }

  g_font10->printBlended( UpperX1, y, a, "MAP SIZE: %s", sizeText );
  y += RowH;
  g_font10->printBlended( UpperX1, y, a, "BOMBS:    %03d", _bombs );
  y += RowH;
}

void CustomGameMenu::handleClick( int x, int y )
{
  if( _state != Active )
    return;

  for( int i = 0; i < 6; ++i ) {

    if( !_buttons[ i ].geometry.contains( x, y ) )
      continue;

    setCurrentButton(i);

    doButton(i);
    break;
  }
}

void CustomGameMenu::handleKey( int key )
{
  if( _state != Active )
    return;

  _keyRepeater.handleKey( key );

  switch( key ) {

  case KeyLeft:
    if( _currentButton == B_NEXT_SIZE ||
	_currentButton == B_MORE_BOMBS ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton - 1 );
    }
    break;

  case KeyRight:
    if( _currentButton == B_PREV_SIZE ||
	_currentButton == B_LESS_BOMBS ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton + 1 );
    }
    break;

  case KeyUp:
    if( _currentButton == B_START ||
	_currentButton == B_BACK ) {

      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton - 1 );
    }
    else if( _currentButton == B_LESS_BOMBS ||
	     _currentButton == B_MORE_BOMBS ) {
      
      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton - 2 );
    }
    break;

  case KeyDown:
    if( _currentButton == B_PREV_SIZE || 
	_currentButton == B_NEXT_SIZE || 
	_currentButton == B_LESS_BOMBS ) {
      
      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton + 2 );
    }
    else if( _currentButton == B_MORE_BOMBS ||
	     _currentButton == B_START ) {
      
      playSFX( SFX_Uncover );
      setCurrentButton( _currentButton + 1 );
    }
    break;
    
  case KeyOk:
    doButton(_currentButton);
    break;

  case KeyMenu:
    g_stateManager.popState();
    break;
  }
}

void CustomGameMenu::onTransition( STM::State* /*prevState*/ )
{
  if( getTransitionState() == TransitionIn ) {

    _currentButton = B_START;
    for( int i = 0; i < 6; ++i ) {
      
      _buttons[ i ].setFocus( i == _currentButton );
      _buttons[ i ].setState( Button::Idle );
    }
    
    setState(FadeIn);
    //setTransitionState(TransitionInDone);
  }
  else {

    g_propertiesFile->setInt( "custom_map.size", _size );
    g_propertiesFile->setInt( "custom_map.bombs", _bombs );

    setState(FadeOut);
    //setTransitionState(TransitionOutDone);
  }
}

int CustomGameMenu::getSize() const
{
  return _size;
}

int CustomGameMenu::getBombs() const
{
  return _bombs;
}

void CustomGameMenu::doButton(int button)
{
  switch( button ) {
      
  case B_PREV_SIZE:
    if( _size > 0 ) {
	
      _buttons[ button ].setState( Button::Ok );
      _size--;
      _bombs = std::min( _bombs, getMaxBombs(_size) );
    }
    else {
	
      _buttons[ button ].setState( Button::Error );
    }
    break;
      
  case B_NEXT_SIZE:
    if( _size < 2 ) {
	  
      _buttons[ button ].setState( Button::Ok );
      _size++;
    }
    else {
	
      _buttons[ button ].setState( Button::Error );
    }
    break;
	
  case B_LESS_BOMBS:
    if( _bombs > 1 ) {
	
      _buttons[ button ].setState( Button::Ok );
      _bombs--;
    }
    else {
	
      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_MORE_BOMBS:
    if( _bombs < getMaxBombs(_size) ) {
	
      _buttons[ button ].setState( Button::Ok );
      _bombs++;
    }
    else {
	
      _buttons[ button ].setState( Button::Error );
    }
    break;

  case B_START:

    _buttons[ button ].setState( Button::Ok );

    g_stateManager.popState();

    if( listener )
      listener->onMenuFinished(B_START);

    break;

  case B_BACK:
    _buttons[ button ].setState( Button::Ok );
    g_stateManager.popState();
    break;
  }
}

void CustomGameMenu::setCurrentButton(int button)
{
  _buttons[_currentButton].setFocus( false );
  _currentButton = button;
  _buttons[_currentButton].setFocus( true );
}

void CustomGameMenu::setState(State state)
{
  _state = state;
  _ticks = 0;
}
