#include "globals.h"
#include "highscores.h"
#include "propertiesfile.h"

#include <pcrecpp.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

Highscores::Highscores()
  : lastPosition( -1 )
{
  assert( !g_highscores );
  g_highscores = this;

  reset();
}

static int getDefaultScoreSeconds( int numBombs, int position )
{
  return ((numBombs * 3) / 10) * 5 + position * 5;
}

int Highscores::getPosition( const GameKey& key, unsigned time ) const
{
  std::map<GameKey, std::vector<Entry> >::const_iterator it = entries.find(key);
  if( it == entries.end() ) {

    for( int i = 0; i < 10; ++i ) {
      
      if( getDefaultScoreSeconds(key.numBombs, i) * 1000 > (int) time )
	return i;
    }

    return -1;
  }
  
  for( int i = 0; i < 10; ++i ) {

    if( (*it).second[ i ].time > time )
      return i;
  }

  return -1;
}

void Highscores::setHighscore( const GameKey& key, const std::string& name, unsigned time )
{
  int pos = getPosition( key, time );
  if( pos < 0 )
    return;
  
  std::map<GameKey, std::vector<Entry> >::iterator it = entries.find(key);                                                                                                                                         
  if( it == entries.end() ) {

    reset(key);
    it = entries.find(key);
  }

  for( int i = 8; i >= pos; --i ) {

    (*it).second[ i + 1 ].name = (*it).second[ i ].name;
    (*it).second[ i + 1 ].time = (*it).second[ i ].time;
  }

  (*it).second[ pos ].name = name;
  (*it).second[ pos ].time = time;

  lastGameKey = key;
  lastPosition = pos;
  lastName = name;
}

void Highscores::reset(const GameKey& key)
{
  std::vector<Entry>& scores = entries[key];
  scores.clear();

  for( int i = 0; i < 10; ++i ) {
    
    scores.push_back(Entry());
    scores.back().name = "RAINBOW PLAINS";
    scores.back().time = 1000 * getDefaultScoreSeconds(key.numBombs, i);
  }
}

void Highscores::reset()
{
  entries.clear();

  reset( Game::getDefaultGameKey( Game::Small ) );
  reset( Game::getDefaultGameKey( Game::Medium ) );
  reset( Game::getDefaultGameKey( Game::Large ) );
}

bool Highscores::load()
{
  assert( g_propertiesFile );

  pcrecpp::RE pattern("^score.key([0-9A-Za-z]+).position(\\d).time$");

  std::list<std::string> keys;
  g_propertiesFile->findKeys(pattern, &keys);
  
  for( std::list<std::string>::const_iterator it = keys.begin();
       it != keys.end(); ++it ) {

    const std::string& keyTime = *it;

    unsigned gameKeyValue;
    int position;
    pattern.PartialMatch(keyTime, pcrecpp::Hex(&gameKeyValue), &position);
    
    char keyName[ 1024 ];
    snprintf( keyName, sizeof( keyName ), "score.key%08X.position%d.name", gameKeyValue, position );

    int time = g_propertiesFile->getInt( keyTime, -1 );
    std::string name = g_propertiesFile->getString( keyName );

    if( !name.size() || time <= 0 )
      continue;

    GameKey gameKey( GameKey::fromUnsigned(gameKeyValue) );
    if( !isValidMapSize( gameKey.width, gameKey.height ) )
      continue;
    
    //setHighscore( gameKey, name, time );
    std::map<GameKey, std::vector<Entry> >::iterator it = entries.find(gameKey);
    if( it == entries.end() ) {

      reset(gameKey);
      it = entries.find(gameKey);
    }
    
    (*it).second[position].name = name;
    (*it).second[position].time = time;
  }

  //lastGameKey = GameKey();

  return true;
}

bool Highscores::save()
{
  assert( g_propertiesFile );

  for( std::map<GameKey, std::vector<Entry> >::const_iterator it = entries.begin();
       it != entries.end(); ++it ) {

    for( int i = 0; i < 10; ++i ) {

      char buffer[ 1024 ];
      snprintf( buffer, sizeof( buffer ), "score.key%08X.position%d.name", (*it).first.toUnsigned(), i );
      g_propertiesFile->setString( buffer, (*it).second[ i ].name );
      snprintf( buffer, sizeof( buffer ), "score.key%08X.position%d.time", (*it).first.toUnsigned(), i );
      g_propertiesFile->setInt( buffer, (*it).second[ i ].time );
    }
  }

  return true;
}

GameKey Highscores::getNextKey(const GameKey& key) const
{
  std::map<GameKey, std::vector<Entry> >::const_iterator it = entries.find(key);
  ++it;

  if( it == entries.end() ) {

    return (*entries.begin()).first;
  }
  else {

    return (*it).first;
  }
}

GameKey Highscores::getPrevKey(const GameKey& key) const
{
  std::map<GameKey, std::vector<Entry> >::const_iterator it = entries.find(key);
  if( it == entries.begin() ) {

    return (*entries.rbegin()).first;;
  }
  else {

    --it;
    return (*it).first;
  }
}
