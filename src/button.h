#ifndef __BUTTON_H__
#define __BUTTON_H__

#include <tmepp/tmepp.h>

#include <string>

#include <stdint.h>

class Button
{
 public:

  enum State {
    
    Idle,
    Ok,
    Error
  };

  enum Neighbour {

    Top,
    Left,
    Bottom,
    Right
  };
  
  Button( const Tme::FloatRect& geometry = Tme::FloatRect(), const std::string& text = std::string(), bool adjustPos = false );
  
  void setText( const std::string& text );

  void update();
  void render( uint8_t alpha );
  void setState( State state );
  void setFocus( bool focus );

  void setNeighbour( Neighbour n, Button* b );

  Tme::FloatRect geometry;
  std::string text;

  Button* neighbours[4];

 private:

  State _state;
  int _ticks;
  
  unsigned _color; // Menu::BoxColor
  bool _focus;
  
  int _x;
  int _y;
  int _cols;
  int _rows;
  float _textX;
  float _textY;

  float _frameAlphaX;
  uint8_t _frameAlpha;
};

#endif // __BUTTON_H__
