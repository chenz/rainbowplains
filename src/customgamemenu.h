#ifndef _CUSTOMGAMEMENU_H_
#define _CUSTOMGAMEMENU_H_

#include "button.h"
#include "keyrepeater.h"
#include "state.h"

class MenuListener;

class CustomGameMenu : public State
{
public:
  
  enum {
    
    B_PREV_SIZE,
    B_NEXT_SIZE,
    B_LESS_BOMBS,
    B_MORE_BOMBS,
    B_START,
    B_BACK,
  };
  
  CustomGameMenu( STM::StateManager* manager );

  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  
  void onTransition( STM::State* prevState );

  int getSize() const;
  int getBombs() const;
  
  MenuListener* listener;

private:

  enum State {

    FadeIn,
    Active,
    FadeOut,
  };

  void doButton(int button);
  void setCurrentButton(int button);
  void setState(State state);

  KeyRepeater _keyRepeater;

  Button _buttons[6];
  int _currentButton;

  int _size;
  int _bombs;

  State _state;
  int _ticks;
};

#endif /* _CUSTOMGAMEMENU_H_ */
