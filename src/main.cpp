#include "bgmplayer.h"
#include "game.h"
#include "globals.h"
#include "highscores.h"
#include "menu.h"
#include "options.h"
#include "pandora.h"
#include "pc.h"
#include "propertiesfile.h"
#include "sprites.h"
#include "state.h"
#include "system.h"
#include "title.h"
#include "wiz.h"

#include <tmepp/tmepp.h>

#include <SDL_mixer.h>

#include <cassert>

static SDL_Joystick* joystick = 0;




void mainLoop()
{
  unsigned time0 = SDL_GetTicks();
  unsigned timeAcc = 0;
  unsigned lastEventTime = time0;

  for( ;; ) {

    g_buttonsPrev = g_buttons;
    g_keysPrev = g_keys;

    bool run = true;

    SDL_Event event;
    while( SDL_PollEvent( &event ) ) {

      lastEventTime = SDL_GetTicks();

      if( !g_stateManager.dispatchSDLEvent( &event ) ) {

	if( event.type == SDL_QUIT ) {

	  run = false;
	  break;
	}
      }
    }

    if( !run )
      break;

    // Current time
    unsigned time1 = SDL_GetTicks();

    if( !UseVSync ) {
      
      unsigned dTime = time1 - time0;
      if( dTime < Period ) {
	
	SDL_Delay( Period - dTime );
	time1 = SDL_GetTicks();
	dTime = time1 - time0;
      }

      // Slow-down hack for transition debugging...
      //SDL_Delay( 200 );
      //time1 = SDL_GetTicks();

      time0 = time1;  
      timeAcc += dTime;
      while( timeAcc >= Period && /*StateManager::instance()->topState()*/ !g_stateManager.isDone() ) {
	
	timeAcc -= Period;
	//StateManager::instance()->update();
	g_stateManager.update();

	if( g_volumeControlTicks > 0 )
	  g_volumeControlTicks--;
      }
    }
    else {
      
      // VSync...

      //StateManager::instance()->update();
      g_stateManager.update();

      if( g_volumeControlTicks > 0 )
	g_volumeControlTicks--;
    }
    
    //if( !StateManager::instance()->topState() )
    //  break;
    if( g_stateManager.isDone() )
      break;

    Tme::Video::startFrame();
    Tme::Video::clear();
    
    //StateManager::instance()->render();
    g_stateManager.render();

#ifdef SYSTEM_WIZ
    if( wiz_isBatteryLow() )
      Tme::Video::blitSprite( SPRITE_BATTERY, ScreenW - 20, ScreenH - 12 );
#endif

    if( g_volumeControlTicks > 0 ) {
      
      const int BoxW = ( 10 + 4 ) * 8;
      const int BoxH = 24;
      const int BoxX = ( ScreenW - BoxW ) / 2;
      const int BoxY = 200;

      Menu::drawBox( BoxX, BoxY, BoxW / 8, BoxH / 8, 
		     Menu::ColorNormal, 255 );

      const int volume = std::max( g_options->getSFXVolume(), g_options->getBGMVolume() );

      char buffer[ 11 ];
      for( int i = 0; i < 11; ++i ) {
	
	buffer[ i ] = ( i < volume ) ? 'I' : '.';
      }
      buffer[ 10  ] = 0;

      g_font8->print( BoxX + 2 * 8, BoxY + 8, buffer );
    }

    #if DEBUG
    g_system->dbgShowStats();
    #endif

    Tme::Video::renderFrame();
    Tme::Video::finishFrame();

    g_system->checkVolume();

    g_bgmPlayer->update();

    const unsigned ScreensaverDelay = 5; // Minutes
    g_system->enableScreensaver( ( time1 - lastEventTime ) >= ScreensaverDelay * 60 * 1000 );

#ifdef SYSTEM_WIZ
    wiz_checkBattery();
    run = run && !wiz_isQuit();
#endif
  }
  
}

int main( int argc, char** argv )
{
  fprintf( stderr, "%s v%s\n", GameTitle, VERSION );

#if defined(SYSTEM_WIZ)
  SystemWiz system;
#elif defined(SYSTEM_PANDORA)
  SystemPandora system;
#else
  SystemPC system;
#endif
  g_system = &system;

  g_propertiesFile = new PropertiesFile;
  if( !g_propertiesFile->load( "../data/properties.dat" ) ) {

    tmeDebugMsg( "PropertiesFile::load" );
  }

  Options options;
  g_options = &options;

  options.load(g_propertiesFile);

  srand( time( 0 ) );

  if( !Tme::Video::init( VIDEO_DRIVER, ScreenW, ScreenH, false, SCREEN_S ) ) {

    tmeErrorMsg( "Tme::Video::init" );
    exit( 1 );
  }

  SDL_WM_SetCaption("Rainbow Plains", 0);

  if( SDL_Init( SDL_INIT_JOYSTICK ) < 0 ) {

    tmeErrorMsg( "SDL_Init: %s", SDL_GetError() );
    Tme::Video::shutdown();
    exit( 1 );
  }
  SDL_JoystickEventState( SDL_ENABLE );
  joystick = SDL_JoystickOpen( 0 );

  //g_audio = false;
  g_audio = true;

  if( SDL_Init( SDL_INIT_AUDIO ) < 0 ) {

    tmeErrorMsg( "SDL_Init: %s", SDL_GetError() );
    g_audio = false;
    //Tme::Video::shutdown();
    //exit( 1 );
  }
  if( Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, AUDIO_S16SYS, 2, 1024 ) < 0 ) {

    tmeErrorMsg( "Mix_OpenAudio: %s", Mix_GetError() );
    g_audio = false;
  }

  for( int i = 0; i < NumSFX; ++i ) {

    g_sfx[ i ] = Mix_LoadWAV( g_sfxFiles[ i ] );
    if( !g_sfx[ i ] ) {

      tmeErrorMsg( "Mix_LoadWAV (%s): %s", g_sfxFiles[ i ], Mix_GetError() );
      //Tme::Video::shutdown();
      //exit( 1 );
    }
  }

  //g_masterVolume = 1;
  setVolume();

  g_bgmPlayer = new BGMPlayer;
  if( !g_bgmPlayer->init() ) {

    tmeErrorMsg( "BGMPlayer::init" );
  }
  else {

    g_bgmPlayer->play();
  }

  Tme::Spriteset* sprites = Tme::loadSpriteset( "../data/sprites.xml" );
  if( !sprites ) {

    tmeErrorMsg( "Tme::loadSpriteset" );
    Tme::Video::shutdown();
    exit( 1 );
  }
  sprites->registerSprites( 0 );
  
  g_font8 = new Tme::SpriteFont( SPRITE_FONT08_00, 8, 8, 60 );
  g_font10 = new Tme::SpriteFont( SPRITE_FONT10_00, 10, 10, 60 );

  Highscores highscores;
  if( !highscores.load() ) {

    tmeDebugMsg( "Highscores::load" );
  }

  Title* title = new Title( &g_stateManager );
  Game* game = new Game( &g_stateManager );
  //StateManager::instance()->pushState( title );
  g_stateManager.pushState( title );

  mainLoop();

  delete title;
  delete game;

  delete g_bgmPlayer;
  g_bgmPlayer = 0;

  if( !highscores.save() ) {

    tmeDebugMsg( "Highscores::save" );
  }

  options.save(g_propertiesFile);

  if( !g_propertiesFile->save( "../data/properties.dat" ) ) {

    tmeErrorMsg( "PropertiesFile::save" );
  }
  delete g_propertiesFile;
  g_propertiesFile = 0;

  if( joystick )
    SDL_JoystickClose( joystick );

  Tme::Video::shutdown();
  return 0;
}
