#ifndef __STATE_H__
#define __STATE_H__

#include <stm/state.h>

#include <SDL.h>

//#include <vector>

class State : public STM::State
{
 public:

  State( STM::StateManager* manager );

  bool onSDLEvent( SDL_Event* event );

  virtual void handleClick( int x, int y ) {}
  virtual void handleKey( int key ) {}

  State* nextState; // Hack, TODO: Implement this proper in libstm
};

//struct State
//{
//  virtual ~State() {}
//
//  virtual void update() {}
//  virtual void render() {}
//  virtual void handleClick( int x, int y ) {}
//  virtual void handleKey( int key ) {}
//
//  virtual void enter() {}
//  virtual void leave() {}
//};

//class StateManager
//{
// public:
//  static StateManager* instance();
//
//  State* topState();
//  void pushState( State* state );
//  void popState();
//
//  void setNextState( State* state );
//
//  void update();
//  void render();
//
// private:
//  StateManager();
//  //std::vector< State* > _stack;
//  
//  State* _stack[ 16 ];
//  int _stackIndex;
//
//  State* _nextState;
//};

#endif // __STATE_H__
