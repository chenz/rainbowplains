#include "button.h"
#include "globals.h"
#include "menu.h"

Button::Button( const Tme::FloatRect& geometry, const std::string& text, bool adjustPos )
  : geometry( geometry )
  , _state( Idle )
  , _ticks( 0 )
  , _focus( false )
{
  _x = (int) geometry.x();
  _y = (int) geometry.y();

  const int wIn = geometry.w();
  const int w = Menu::roundSize(wIn);
  this->geometry.setW( w );
  if( wIn != w && adjustPos ) {

    _x += (w - wIn) / 2;
    this->geometry.setX(_x);
  }

  const int hIn = geometry.h();
  const int h = Menu::roundSize(hIn);
  this->geometry.setH( h );
  if( hIn != h && adjustPos ) {

    _y += (h - hIn) / 2;
    this->geometry.setY(_y);
  }

  _cols = w / 8;
  _rows = h / 8;
 
  for( int i = 0; i < 4; ++i )
    neighbours[ i ] = 0;

  setText( text );
}

void Button::setText( const std::string& text )
{
  this->text = text;

  const int FontW = 10;
  const int FontH = 10;

  _textX = _x + ( _cols * 8 - FontW * (int) text.size() ) / 2;
  _textY = _y + ( _rows * 8 - FontH ) / 2;
}

void Button::render( uint8_t alpha )
{
  Menu::drawBox( _x, _y, _cols, _rows, (Menu::BoxColor) _color, alpha );
  
  if( _focus && !g_touchMode )
    Menu::drawFrame( _x, _y, _cols, _rows, std::min( alpha, _frameAlpha ) );
  
  //g_font8->printBlended( _textX, _textY, alpha, text.c_str() );
  g_font10->printBlended( (int) _textX, (int) _textY, alpha, text.c_str() );
}

void Button::update()
{
  _ticks++;
  
  if( _focus ) {
    
    _frameAlphaX += 12.0f / (float) FPS;
    _frameAlpha = (uint8_t)( 255.0f - 100.0f + sinf( _frameAlphaX ) * 100.0f );
  }
  
  const int HighlightTicks = 300 / Period;
  
  switch( _state ) {

  case Idle:
    break;

  case Ok:
  case Error:
    if( _ticks >= HighlightTicks ) {
      
      setState( Idle );
    }
    break;
  }
}

void Button::setState( State state )
{
  _ticks = 0;
  _state = state;

  switch( state ) {

  case Idle:
    _color = Menu::ColorNormal;
    break;
  case Ok:
    playSFX( SFX_Uncover );
    _color = Menu::ColorGreen;
    break;
  case Error:
    playSFX( SFX_Buzzer );
    _color = Menu::ColorRed;
    break;
  }
}

void Button::setFocus( bool focus )
{
  _focus = focus;
  _frameAlphaX = 0.0f;
}

void Button::setNeighbour( Neighbour n, Button* b )
{
  switch( n ) {

  case Top:
    neighbours[ Top ] = b;
    b->neighbours[ Bottom ] = this;
    break;
  case Bottom:
    neighbours[ Bottom ] = b;
    b->neighbours[ Top ] = this;
    break;
  case Left:
    neighbours[ Left ] = b;
    b->neighbours[ Right ] = this;
    break;
  case Right:
    neighbours[ Right ] = b;
    b->neighbours[ Left ] = this;
    break;
  }
}
