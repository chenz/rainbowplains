#ifndef _SOUNDMENU_H_
#define _SOUNDMENU_H_

#include "button.h"
#include "keyrepeater.h"
#include "state.h"

#include <string>

class MenuListener;

class SoundMenu : public State
{
public:
  
  enum {
    
    B_SFX_TOGGLE_PREV,
    B_SFX_TOGGLE_NEXT,
    B_SFX_VOLUME_PREV,
    B_SFX_VOLUME_NEXT,
    B_BGM_TOGGLE_PREV,
    B_BGM_TOGGLE_NEXT,
    B_BGM_VOLUME_PREV,
    B_BGM_VOLUME_NEXT,
    B_BGM_MODE_PREV,
    B_BGM_MODE_NEXT,
    B_BGM_SKIP,
    B_BACK,
    NUM_BUTTONS,
  };
  
  SoundMenu( STM::StateManager* manager );

  void update();
  void render();
  void handleClick( int x, int y );
  void handleKey( int key );
  
  void onTransition( STM::State* prevState );

  MenuListener* listener;

private:

  enum State {

    FadeIn,
    Active,
    FadeOut,
  };

  void doButton(int button);
  void setCurrentButton(int button);
  void setState(State state);

  Button _buttons[NUM_BUTTONS];
  int _currentButton;

  State _state;
  int _ticks;
};

#endif /* _SOUNDMENU_H_ */
