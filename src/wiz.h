#ifndef __WIZ_H__
#define __WIZ_H__

#if defined(SYSTEM_WIZ)

#include "system.h"

enum WizButtons
{
  WizUpButton = 0x00,
  WizUpLeftButton,
  WizLeftButton,
  WizDownLeftButton,
  WizDownButton,
  WizDownRightButton,
  WizRightButton,
  WizUpRightButton,
  WizMenuButton,
  WizSelectButton,
  WizLeftShoulderButton,
  WizRightShoulderButton,
  WizAButton,
  WizBButton,
  WizXButton,
  WizYButton,
  WizVolPlusButton,
  WizVolMinusButton
};

bool wiz_isBatteryLow();
bool wiz_isQuit();
void wiz_checkBattery();

class SystemWiz : public AbstractSystem
{
public:
  int mapJoyButton( unsigned button ) const;
  int mapKey( unsigned key ) const;
  void enableScreensaver( bool on );
  void dbgShowStats();
};

#endif // defined(SYSTEM_WIZ)

#endif // __WIZ_H__
