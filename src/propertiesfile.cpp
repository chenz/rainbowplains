#include "propertiesfile.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static bool isBlank( int c )
{
  return 
    c == ' ' ||
    c == '\t' ||
    c == '\r' ||
    c == '\n';
}

static char* trim( char* string )
{
  char* start = string;
  while( *start && isBlank( *start ) )
    start++;
  if( !*start )
    return start;

  char* end = &start[ strlen( start ) - 1 ];
  while( end > start && isBlank( *end ) )
    *end-- = 0;
 
  return start;
}

static bool parseLine( char* line, char** lstring, char **rstring )
{
  char* comment = strchr( line, '#' );
  if( comment )
    *comment = 0;

  char* assign = strchr( line, '=' );
  if( !assign )
    return false;
  
  *assign++ = 0;
  
  *lstring = trim( line );
  *rstring = trim( assign );

  return **lstring && **rstring;
}

PropertiesFile::PropertiesFile()
  : _changed( false )
{
}


bool PropertiesFile::load( const std::string& filename )
{
  FILE* fp = fopen( filename.c_str(), "rb" );
  if( !fp ) {

    perror( "fopen" );
    return false;
  } 

  char buffer[ 1024 ];
  while( fgets( buffer, sizeof( buffer ), fp ) ) {

    char* lstring = 0;
    char* rstring = 0;

    if( parseLine( buffer, &lstring, &rstring ) ) {

      _data[ lstring ] = rstring;
    }
  }

  fclose( fp );
  return true;
}

bool PropertiesFile::save( const std::string& filename )
{
  if( !_changed )
    return true;

  FILE* fp = fopen( filename.c_str(), "w+" );
  if( !fp ) {

    perror( "fopen" );
    return false;
  } 

  for( std::map< std::string, std::string >::const_iterator it = _data.begin();
       it != _data.end(); ++it ) {

    fprintf( fp, "%s=%s\r\n", (*it).first.c_str(), (*it).second.c_str() );
  }

  fclose( fp );
  sync();
  _changed = false;
  return true;
}

std::string PropertiesFile::getString( const std::string& key, const std::string& fallback ) const
{
  std::map< std::string, std::string >::const_iterator it = _data.find( key );
  if( it == _data.end() )
    return fallback;

  return (*it).second;
}

int PropertiesFile::getInt( const std::string& key, int fallback )
{
  std::map< std::string, std::string >::const_iterator it = _data.find( key );
  if( it == _data.end() )
    return fallback;

  return atoi( (*it).second.c_str() );
}

void PropertiesFile::setString( const std::string& key, const std::string& value )
{
  std::map< std::string, std::string >::const_iterator it = _data.find(key);
  _changed = _changed || (it == _data.end()) || ((*it).second != value);
  
  _data[key] = value;
}

void PropertiesFile::setInt( const std::string& key, int value )
{
  char buffer[ 32 ];
  snprintf( buffer, sizeof( buffer ), "%d", value );
  setString( key, buffer );
}

void PropertiesFile::findKeys( const std::string& start, std::list< std::string >* keys ) const
{
  for( std::map< std::string, std::string >::const_iterator it = _data.begin();
       it != _data.end(); ++it ) {

    if( (*it).first.find( start ) == 0 )
      keys->push_back( (*it).first );
  }
}

void PropertiesFile::findKeys( const pcrecpp::RE& pattern, std::list< std::string >* keys ) const
{
  for( std::map< std::string, std::string >::const_iterator it = _data.begin();
       it != _data.end(); ++it ) {

    if( pattern.PartialMatch((*it).first) )
      keys->push_back( (*it).first );
  }
}
