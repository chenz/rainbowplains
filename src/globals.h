#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#include <stm/statemanager.h>
#include <tmepp/tmepp.h>

#include <SDL_mixer.h>

class AbstractSystem;
class BGMPlayer;
class Game;
class Highscores;
class Options;
class PropertiesFile;
class Title;

extern AbstractSystem* g_system;

extern const char* GameTitle;

extern PropertiesFile* g_propertiesFile;
extern Options* g_options;

extern Tme::SpriteFont* g_font8;
extern Tme::SpriteFont* g_font10;

//const unsigned FPS = 10;
//const bool UseVSync = false;

const unsigned FPS = 30;
const bool UseVSync = false;

//const unsigned FPS = 60;
//const bool UseVSync = false;

const unsigned Period = 1000 / FPS;

#if defined(SYSTEM_WIZ)
  #define SCREEN_W 320
  #define SCREEN_S 1
  #define VIDEO_DRIVER Tme::VideoWiz
#elif defined(SYSTEM_PANDORA)
  #define SCREEN_W 400
  #define SCREEN_S 2
  #define VIDEO_DRIVER Tme::VideoWiz
#elif defined(SYSTEM_PC)
  #define SCREEN_W 320
  #define SCREEN_S 2
  #define VIDEO_DRIVER Tme::VideoGL
//  #define SCREEN_W 400
//  #define SCREEN_S 2
//  #define VIDEO_DRIVER Tme::VideoGL
#else
  #error No system defined!
#endif

const int ScreenW = SCREEN_W;
const int ScreenH = 240;

const int MenuBarH = 12;

const int TileSizeSmallMap = 32;
const int TileSizeMediumMap = 24;
const int TileSizeLargeMap = 16;

//const int MaxBombsSmallMap = (ScreenW / TileSizeSmallMap) * ((ScreenH - MenuBarH) / TileSizeSmallMap) - 1;
//const int MaxBombsMediumMap = (ScreenW / TileSizeMediumMap) * ((ScreenH - MenuBarH) / TileSizeMediumMap) - 1;
//const int MaxBombsLargeMap = (ScreenW / TileSizeLargeMap) * ((ScreenH - MenuBarH) / TileSizeLargeMap) - 1;

extern unsigned g_buttons;
extern unsigned g_buttonsPrev;

enum InputEvent {
  
  KeyUp,
  KeyDown,
  KeyLeft,
  KeyRight,
  KeyOk,
  KeyBack,
  KeyFlag,
  KeyMenu,
  KeyVolumePlus,
  KeyVolumeMinus,
  KeyCheat,
  MaxKeys
};

extern unsigned g_keys;
extern unsigned g_keysPrev;

bool flagKeyPressed();

extern bool g_audio;

enum SFXType {

  SFX_Uncover,
  SFX_UncoverBulk,
  SFX_Explosion1,
  SFX_Buzzer,
  SFX_Won,
  NumSFX
};

extern Mix_Chunk* g_sfx[ NumSFX ];
extern const char* g_sfxFiles[ NumSFX ];

void playSFX( SFXType type );

//extern int g_masterVolume; // 0 - 10
extern int g_volumeControlTicks;
void setVolume();
void volumeUp();
void volumeDown();

extern BGMPlayer* g_bgmPlayer;

float clamp( float x, float a, float b );
Uint8 getFadeInAlpha( float x, float n );
Uint8 getFadeOutAlpha( float x, float n );

extern Game* g_game;
extern Title* g_title;

extern bool g_touchMode;

extern Highscores* g_highscores;

enum BlinkSpeed {

  BlinkFast,
  BlinkMedium,
  BlinkSlow,
};

bool getBlink( BlinkSpeed speed, int ticks );

int getMaxBombs(int size);
int getNumBombsDefault(int size);

int getMapWidth( int size );
int getMapHeight( int size );

bool isValidMapSize( int w, int h );

extern STM::StateManager g_stateManager;

#endif // __GLOBALS_H__
