#ifndef __TITLE_H__
#define __TITLE_H__

#include "highscorescreen.h"
//#include "hue.h"
#include "menu.h"
#include "state.h"

#include <stdint.h>

class CustomGameMenu;
class SoundMenu;

struct Title : public State, public MenuListener
{
  enum State
  {
    FadeIn,
    FadeInLogo,
    Idle,
    FadeOut
  };

  Title( STM::StateManager* manager );
  ~Title();
  
  void update();
  void render();

  //void enter();
  void onTransition( STM::State* prevState );

  void onMenuFinished( int index );

  void setState( State state );
  //void createRandomCheckboard();
  //void shiftColors();
  void renderBackground();

  //uint8_t* tiles;
  //unsigned mapW;
  //unsigned mapH;

  Uint8 alpha;

  int _ticks;
  
  Menu* menu;

  State state;

  CustomGameMenu* _customGameMenu;
  HighscoreScreen* _highscoreScreen;
  SoundMenu* _soundMenu;

  //Hue _backgroundHues[ 2 ];
  unsigned _backgroundHueTicks;
  int _sineTicks;

  //float scrollX;
};

#endif // __TITLE_H__
