#ifndef __KEYREPEATER_H__
#define __KEYREPEATER_H__

#include "globals.h"

class State;

class KeyRepeater
{
public:
  KeyRepeater( State* state, const unsigned initialDelay, const unsigned repeatDelay );

  void handleKey( unsigned key );
  void update();

private:
  State* _state;
  const unsigned _initialDelay;
  const unsigned _repeatDelay;

  unsigned _ticks[ MaxKeys ];
};

#endif // __KEYREPEATER_H__
