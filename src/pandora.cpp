// Pandora implementation of system interface...

#if defined(SYSTEM_PANDORA)

#include "globals.h"
#include "pandora.h"

#include <SDL.h>

int SystemPandora::mapJoyButton( unsigned /*button*/ ) const
{
  return -1;
}

int SystemPandora::mapKey( unsigned key ) const
{
  switch( key ) {

  case SDLK_UP:
    return KeyUp;
  case SDLK_DOWN:
    return KeyDown;
  case SDLK_LEFT:
    return KeyLeft;
  case SDLK_RIGHT:
    return KeyRight;
  case SDLK_HOME:
  case SDLK_END:
    return KeyOk;
  case SDLK_PAGEDOWN:
  case SDLK_PAGEUP:
    return KeyBack;
  case SDLK_LALT:
    return KeyMenu;
  }

  return -1;
}

void SystemPandora::enableScreensaver( bool /*on*/ )
{
}

void SystemPandora::dbgShowStats()
{
}

#endif // defined(SYSTEM_PANDORA)
