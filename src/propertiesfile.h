#ifndef __PROPERTIESFILE_H__
#define __PROPERTIESFILE_H__

#include <pcrecpp.h>

#include <list>
#include <map>
#include <string>

class PropertiesFile
{
 public:
  PropertiesFile();

  bool load( const std::string& filename );
  bool save( const std::string& filename );

  std::string getString( const std::string& key, const std::string& fallback = std::string() ) const;
  int getInt( const std::string& key, int fallback = -1 );

  void setString( const std::string& key, const std::string& value );
  void setInt( const std::string& key, int value );

  void findKeys( const std::string& start, std::list< std::string >* keys ) const;

  void findKeys( const pcrecpp::RE& pattern, std::list< std::string >* keys ) const;

 private:
  std::map< std::string, std::string > _data;
  bool _changed;
};

#endif // __PROPERTIESFILE_H__
