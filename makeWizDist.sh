#!/bin/bash

set -e

VERSION=$(make -s -C src version)

make -C lib
make -C src

rm -rf dist/rainbowplains
mkdir -p dist/rainbowplains/bin
mkdir -p dist/rainbowplains/data
mkdir -p dist/rainbowplains/bgm

cp data/*.wav dist/rainbowplains/data/
cp data/*.xml dist/rainbowplains/data/
cp data/*.png dist/rainbowplains/data/
cp wiz/icon.png dist/rainbowplains/data/

cp src/rainbowplains dist/rainbowplains/bin/
arm-linux-strip -s dist/rainbowplains/bin/rainbowplains
cp wiz/pollux_set dist/rainbowplains/bin/

cp wiz/rainbowplains.gpe dist/rainbowplains/
cp wiz/rainbowplains.ini dist/

cd dist
zip -r ../rainbowplains-wiz-${VERSION}.zip rainbowplains rainbowplains.ini
tar vjcf ../rainbowplains-wiz-${VERSION}.tar.bz2 rainbowplains rainbowplains.ini
